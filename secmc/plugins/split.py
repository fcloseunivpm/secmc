import os 
import jinja2
import subprocess

from past.builtins import basestring
from secmc.plugins import Command, CommandAbort

def split_strip(tosplit, symbol):
    splitted = tosplit.split(symbol)
    stripped = map(lambda s: s.strip(), splitted)
    return stripped

def as_float(value):

    if not isinstance(value, float):
        value = float(value)

    return value

class SplitAndJoin(Command):

    description = "Split the model checking problem into several distinct instances, exploring the possible combinations of one or more *independent* aleatory variable"

    PROB_FORMAT = "{0:.15f}"

    PRISM_COMMAND = "prism"

    def get_args_parser(self):
        parser = super(SplitAndJoin, self).get_args_parser()

        parser.add_argument("--var", action="append", nargs="*", help="a string specifying the variable name, possible values, and their probabilities, with the format: <name> = <prob>:<value> + ... + <prob>:<value>")


        parser.add_argument("template", type=str, help="the path of the input template")
        parser.add_argument("model", type=str, help="the (prefix of the) path of the output model")

        parser.add_argument("ctx_var", type=str, nargs="*", help="a variable assignment for the context of the template")

        return parser

    def get_vars(self):

        # TODO instead of using splitting, consider using regexp; at least
        # catch exceptions due to ill-formed expressions
    
        if not self._args:
            raise Exception("You must first parse the arguments")

        vars = {}

        var_args = self.get_arg("var") or []
        self.logger.debug("Var args: %s" % var_args)

        for var_spec in var_args:
            self.logger.debug("Var spec: %s" % var_spec)
            (var_name, var_values_spec) = split_strip(var_spec,"=")
    
            vars[var_name] = {}
            sum_prob = 0.0

            # split based on + and remove leading/trailing spaces
            var_values = split_strip(var_values_spec, "+")
 
            for curr_val in var_values:
                (prob, val) = split_strip(curr_val, ":")
                vars[var_name][val] = prob
                sum_prob = sum_prob + float(prob)

            if sum_prob != 1.0:
                raise Exception("The specified values for variable '%s' have total probabilities different from 1 ('%s')" % (var_name, var_values_spec))

        return vars

    def get_combinations(self, vars):
    
        combinations = [({},1.0)]

        for (new_var,new_val_spec) in vars.items():
            # every new variable we iterate, cause to ``consume'' the
            # previous combinations and replace them with new combinations:
            # the latter are obtained by ``multiplying'' the former by the
            # possible values the new variable

            self.logger.debug("Combinations: %s" % combinations)

            # for each existing combination, create a list of 
            # new combinations ...
            new_combinations = []

            for (old_comb,old_prob) in combinations:
                # consume the existing combinations, one by one ...
                assert isinstance(old_comb, dict)
                assert isinstance(old_prob, float)
                assert old_prob >= 0 and old_prob <= 1

                self.logger.debug("New variable spec: %s -> %s" % (new_var, new_val_spec))

                for (val,prob) in new_val_spec.items():
                    # add a new combinations by taking the old assignments, and 
                    # adding the assignment of the new variable ...
                    new_comb = old_comb.copy()
                    new_prob = old_prob * as_float(prob)

                    new_comb[new_var] = val
                    new_combinations.append((new_comb, new_prob))

            # replace old combinations by new ones ...
            combinations = new_combinations
    
        self.logger.debug("Final combinations: %s" % combinations)
        return combinations

    def get_context(self):

        ctx = {}
        ctx_vars = self.get_arg("ctx_var")

        self.logger.debug("ctx var: %s" % ctx_vars)

        for var in ctx_vars:
            (var_name, var_value) = split_strip(var, "=")
            ctx[var_name] = var_value

        return ctx

    def sanity_check(self, combinations, vars):
        """
        This method is left for double check, but actually it is useless once it
        is proven that the method get_combinations(...) is correct; perhaps this
        method should belong to a suite test (TODO)
        """
        
        num_expected = 1
        for (var_name,var_values) in vars.items():
            num_expected = num_expected * len(var_values)

        sum_prob = 0.0
        num_comb = len(combinations)
        for (comb, prob) in combinations:
            
            sum_prob = sum_prob + prob

        if num_expected != num_comb:
            raise Exception("Expected %s combinations. Found: %s" % (num_expected, num_comb))

        if sum_prob != 1.0:
            raise Exception("The computed combinations should have probabilities that sum up to 1.0. Now you have %s combination/s whose probability sum equals %s" % (num_comb, sum_prob))


    def run (self, template=None, out_model=None, vars=None, ctx=None):

        template = template or self.get_arg("template")

        out_model = out_model or self.get_arg("model")

        vars = vars or self.get_vars()
    
        ctx = ctx or self.get_context()

        self.logger.info("Template: %s, Model: %s, Vars: %s, Context: %s" % (template, out_model, vars, ctx))

        combinations = self.get_combinations(vars)
        num_combinations = len(combinations)
        self.logger.info("Total number of combinations: %s" % num_combinations)

        self.sanity_check(combinations, vars)

        final_prob = 0.0
        for (comb, init_prob) in combinations:
            assert isinstance(comb, dict)
            assert isinstance(init_prob, float)
            assert init_prob >= 0 and init_prob <= 1

            out_prob = self.solve_pmcp_instance(template, out_model, comb, ctx)

            final_prob = final_prob + (out_prob * init_prob)

        self.store_result(final_prob)

    def get_suffix(self, combination):

        assert isinstance(combination, dict)
        
        suffix_parts = []
    
        for var_name in sorted(combination.keys()):
            value = combination[var_name]
            suffix_parts.append("%s_%s" % (var_name,value))

        return "_".join(suffix_parts)


    def solve_pmcp_instance(self, template, model_filename, combination, ctx):

        assert isinstance(template, basestring)
        assert isinstance(model_filename, basestring)
        assert isinstance(combination, dict)
        assert isinstance(ctx, dict)

        suffix = self.get_suffix(combination)

        # add the variable values to the context
        ctx.update(combination)

        (model_prefix, ext) = os.path.splitext(model_filename)

        model = "%s_%s%s" % (model_prefix, suffix, ext)

        cmd = self.registry.get("create_model")

        if not cmd:
            raise CommandAbort("Cannot find plugin 'create_model'")

        assert not os.path.exists(model)

        cmd.run(template=template, out_model=model, ctx=ctx)

        assert os.path.exists(model)
    
        return 0.0

    def store_result(self, final_prob):
        
        assert isinstance(final_prob, float)
        assert final_prob >= 0 and final_prob <= 1

        # TODO the best has yet to come ...

            
