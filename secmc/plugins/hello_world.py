from secmc.plugins import Command

class HelloWorld(Command):
    
    description = "this is a very useful command"

    def get_args_parser(self):

        parser = super(HelloWorld, self).get_args_parser()
        parser.add_argument("your_name", type=str, help="please, specify your name")
        parser.add_argument("--dry", action="store_true", help="wheather is dry? if not, we assume it's wet")
        return parser

    def run(self):
    
        your_name = self.get_arg("your_name")
        print ("Hi %s, this is a hello world!" % your_name)

        if self.get_arg("dry"):
            print ("It's dry today, isn't it?")
        else:
            print ("It's wet today, isn't it?")

        

    
