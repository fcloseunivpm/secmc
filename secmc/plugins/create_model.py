import os
import random
import jinja2
import sys
import subprocess
import yaml

from secmc.plugins import Command, CommandAbort


def filter_exp(base, exp):
    return float(base) ** exp

def split_strip(tosplit, symbol):
    splitted = tosplit.split(symbol)
    stripped = map(lambda s: s.strip(), splitted)
    return stripped


class CreateModel(Command):

    description = "Generate a probabilistic model starting from a template"

    PROB_FORMAT = "{0:.15f}"

    PRISM_COMMAND = "prism"

    def get_args_parser(self):

        parser = super(CreateModel, self).get_args_parser()

        parser.add_argument("template", type=str, help="the path of the input template")
        parser.add_argument("model", type=str, help="the path of the output model")

        parser.add_argument("ctx_var", type=str, help="a YAML-styled string describing a dictionary of pairs 'name:value' (e.g. { a : 1, b : 2 }). Opening and closing brackets are optional.")

        return parser

    def run(self, template=None, out_model=None, ctx=None):

        if not ctx:
            ctx = self.get_context()
        else:
            self._args = ctx

        if not template:
            template = self.get_arg("template")

        if not out_model:
            out_model = self.get_arg("model")

        self.render(template, out_model, ctx)


    def get_context(self):

        ctx_vars = self.get_arg("ctx_var")
 
        if not ctx_vars.startswith("{"):
            ctx_vars = "{" + ctx_vars
        if not ctx_vars.endswith("}"):
            ctx_vars = ctx_vars + "}"

        ctx = yaml.load(ctx_vars)   

        self.logger.debug("Parsed context: %s" % ctx)

        return ctx


    def render(self, template, out_file_path, ctx):

        template_path = os.path.dirname(template) or "."
        template_name = os.path.basename(template) 

        if not out_file_path:
            out_file_path = os.path.splitext(os.path.basename(self.get_arg("template")))[0]
            if not out_file_path.endswith(".prism"):
                out_file_path = out_file_path + ".prism"

        if os.path.exists(out_file_path) and not self.get_arg("force"):
            raise CommandAbort("The output file already exists '%s'. Either delete it or retry using the -f/--force flag" % out_file_path)

    
        if not os.path.exists(template_path):
            msg = "The template path '%s' must be a valid directory" % template_path
            raise CommandAbort(msg)
    
        t_loader = jinja2.FileSystemLoader(searchpath=template_path)
        t_env = jinja2.Environment(loader=t_loader)
    
        # add custom filters
        t_env.filters["myexp"] = filter_exp
        # finished adding filters
    
        t = t_env.get_template(template_name)
    
        try:
            model = t.render(ctx)
        except jinja2.exceptions.UndefinedError as e:
            raise CommandAbort("Some parameter in the template has not been assigned a value: %s" % e)

        self.print_model(model, out_file_path)
        #self.check_syntax(out_file_path)
        

    def print_model(self, model, out_file_path):

        try:
            with open(out_file_path, "w+") as out_file:
                self.logger.debug("Generate model: %s" % out_file_path)
                out_file.write(model)
        except Exception as e:
            raise CommandAbort("Error writing model file: %s. Details: %s" % (out_file_path, e))


    def check_syntax(self, model_path):

        proc = subprocess.Popen("which %s" % self.PRISM_COMMAND, stdout=subprocess.PIPE, shell=True)
        (prism_path, err) = proc.communicate()

        self.logger.debug("Found prism binary: %s" % prism_path)

        if err:
            raise CommandAbort("Cannot find prism binary")

        cli = "%s %s" % (prism_path.strip(), model_path)
        proc = subprocess.Popen(cli, stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()

        if proc.returncode != 0:
            self.logger.error("Syntax error in generated model:\n\n%s" % out)
            raise CommandAbort("Syntax error in generated model")

        self.logger.debug("Syntax of model ('%s') is correct" % model_path)

