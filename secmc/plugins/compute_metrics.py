from secmc.plugins import Command, CommandAbort
import os
import math
from decimal import Decimal, Context, getcontext

class ComputeMetrics(Command):
    """
    This command computes the equivocation and other security metrics 
    starting from the probability of an attack that breaks confidentiality
    to the current channel. The command takes as input the file produced by 
    the plugins.ComputeProbability command.
    """

    description = "Compute the equivocation and random padding values given a table of probabilities of attack"

    def __init__(self, *args, **kwargs):
    
        super(ComputeMetrics, self).__init__(*args, **kwargs)
 
        # get the decimal context (required for the operations with arbitary    
        # precision in the methods of this class)       
        self.ctx = getcontext()

    def get_args_parser(self):
        parser = super(ComputeMetrics, self).get_args_parser()
       
        parser.add_argument("in_table", type=str, help="the path of the input table of probabilities")
        parser.add_argument("out_table", type=str, help="the path of the output table of equivocations and random paddings", nargs="?")

        return parser

    def log2(self, arg):
        """
        log_2(X) = log_10(X) / log_10(2)
        """
    
        assert isinstance(arg, Decimal)
        assert isinstance(self.ctx, Context)

        res = self.ctx.divide(self.ctx.log10(arg), self.ctx.log10(Decimal("2")))

        assert isinstance(res, Decimal)
        return res

    def equivocation(self, prob):

        assert isinstance(self.ctx, Context)
        assert isinstance(prob, Decimal)

        # see work by Massey
#        res = math.log(Decimal(math.e) * (1 / prob - 1), 2)
        res = self.log2(self.ctx.multiply(Decimal(math.e), 
            self.ctx.divide(1,prob) - 1))

        assert isinstance(res, Decimal)
        return res

    def padding(self, equiv, k, lenslice): #(equiv,k,q):
        """
        y = z - x - alpha = (l * q * k) - 2 * x (assuming alpha = x)
        """
        assert isinstance(equiv, Decimal)
        assert isinstance(k, Decimal)
        assert isinstance(lenslice, Decimal)

        assert isinstance(self.ctx, Context)

#        res = (lenslice * k) - (2 * equiv)
        res = self.ctx.multiply(lenslice,k) - \
            self.ctx.multiply(Decimal("2"),equiv)

        assert isinstance(res, Decimal)

        return res


    def run(self):

        from secmc.plugins.run_batch import RunBatch

        # TODO customize path
        in_table_path = self.get_arg("in_table")

        (file_name, ext) = os.path.splitext(in_table_path)

        out_table_path = self.get_arg("out_table") or file_name + "_equivocation.csv"
    
        if os.path.exists(out_table_path) and not self.get_arg("force"):
            raise CommandAbort("The output file '%s' already exist. Either force rewriting or delete it." % out_table_path)

    
        try:
            with open(in_table_path,"r") as in_table:
                with open(out_table_path,"w+") as out_table:
        
                    out_table.write("#label,n,probability,k1,k2,time(sec),lenslice,l,q,k,equivocation,padding,total_size,ratio1(padding/equiv),ratio2(padding/total_size)\n")
        
                    for line in in_table:
        
                        line = line.strip()
        
                        if line.startswith("#"):
                            # skip comment lines
                            continue
                        (label,n,prob,k1,k2,time,lenslice,l,q) = line.split(",")

                        q = RunBatch.compute_q(int(n), int(k1), int(k2))

#                        curr_k = int(float(int(k1) + int(k2)) / 2)
                        curr_k = self.ctx.divide(Decimal(k1) + Decimal(k2),
                                Decimal("2"))
            
                        try:
                            equiv = self.equivocation(Decimal(prob))
                            pad = self.padding(equiv, curr_k, Decimal(lenslice))
                            tot_size = Decimal(math.floor(Decimal(lenslice))) * Decimal(curr_k)

                            ratio1 = self.ctx.divide(pad, equiv) # division between Decimal's
        
                            if tot_size > 0:
                                ratio2 = self.ctx.divide(pad, tot_size) # division between Decimal's
                            else:
                                ratio2 = "NaN"

                            tot_size = str(tot_size)
                            equiv = str(equiv)
                            pad = str(pad)
                            ratio1 = str(ratio1)
                            ratio2 = str(ratio2)
                            
                        except ZeroDivisionError:

                            self.logger.warning("Division by zero in formulas")
                            equiv = "NaN"
                            pad = "NaN"
                            tot_size = "NaN"
                            ratio1 = "NaN"
                            ratio2 = "NaN"

                        q = str(q)
                        curr_k = str(curr_k)

                        row = [label,n,prob,k1,k2,time,lenslice,l,q,curr_k,equiv,pad,tot_size,ratio1,ratio2]
                        out_line = ",".join(row) + "\n"
                        out_table.write(out_line)
        
        except Exception as e:
            self.logger.error("Error reading or writing table")
            self.logger.exception(e)

