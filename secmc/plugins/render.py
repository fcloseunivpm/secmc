import os
import subprocess

from secmc.plugins import Command, CommandAbort

class Render(Command):

    COMMAND = "octave"
    CLI_ARGS = "--persist --no-gui"

    def get_args_parser(self):
    
        parser = super(Render, self).get_args_parser()
    
        parser.add_argument("graph", type=str, help="the path of the graph to render")
        return parser
    

    def run(self, out_file_path=None):

        if not out_file_path:
            out_file_path = self.get_arg("graph")

        # check octave exists in the system
        check_octave = subprocess.call("which %s" % self.COMMAND, shell=True)

        if check_octave != 0:
            raise CommandAbort("The command '%s' is needed to render the graph. Cannot find it in the system" % self.COMMAND)

        cli = "%s %s %s" % (self.COMMAND, self.CLI_ARGS, out_file_path)

        self.logger.info("Run: '%s'" % cli)

        subprocess.call(cli, shell=True)


