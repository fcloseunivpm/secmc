from io import IOBase
import os
import traceback
from collections import deque
import argparse
import logging
import sys
if os.name == 'posix' and sys.version_info[0] < 3:
    import subprocess32 as subprocess
else:
    import subprocess
import errno
import re
import time
import glob
import asteval
import math
import yaml
import types
import jinja2
from decimal import Decimal, getcontext, Context
import pydoc
import imp
from past.builtins import basestring, xrange
from secmc.plugins import CommandAbort, DaemonCommand

ITERATE_FUNCTIONS = {
    "uniform": lambda from_val,to_val,min_val,max_val: lambda i: (Decimal(max_val)-Decimal(min_val))/(Decimal(to_val)-Decimal(from_val)),
    "linear": lambda from_val,to_val,min_val,max_val: lambda i: (i-Decimal(min_val))*((Decimal(max_val)-Decimal(min_val))/(Decimal(to_val)-Decimal(from_val))),
}

##class SmartInterpreter(asteval.Interpreter):
##
##    def eval(self, *args, **kwargs):
##
##        res = None
##        print("intercettato(%s, %s)" % (args, kwargs))
##        res = super(SmartInterpreter, self).eval(*args, **kwargs)
##        print("dopo...")
##        
##
##        print "return: %s" % res
##        return res
##                

def interpret(interpreter, expr):
    res = None
    try:
        res = interpreter(expr)
    finally:
        for err in interpreter.error:
            traceback.print_exception(*err.exc_info)

    return res
            

class BatchTimeoutException(Exception):
    pass

def bounded_int(min=None, max=None):

    if min == None and max == None:
        raise Exception("You must specify either a minimum or a maximum value (or both)")

    def _bounded_int(value):
        value = int(value) 

        if min != None and value < min:
            raise argparse.ArgumentTypeError("Minimum value is %s" % min)
        if max != None and value > max:
            raise argparse.ArgumentTypeError("Maximum value is %s" % max)

        return value


def validate_prob_list(prob_spec, param_name, prob_list_len, allowed_sum=0.0):

    assert isinstance(prob_spec, str)
    assert isinstance(param_name, str)
    assert isinstance(prob_list_len, int)
    assert isinstance(allowed_sum, float)

    prob_list = prob_spec.split(",")
    prob_list_len = len(prob_list)
    if prob_list_len != int(prob_list_len):
        raise CommandAbort("The length of parameter '%s' (%s) should be %s" % (param_name, prob_list_len, prob_list_len))

    fact = 1000
    compare_sum = float(allowed_sum) * fact
    sum = 0.0

    for curr_p in prob_list:
        value = float(curr_p)

        sum = sum + (value * fact)

        if value < 0.0 or value > 1.0:
            raise CommandAbort("The parameter '%s' should contain only probabilities in the interval [0,1]. Found: %s" % (param_name, value,))

    if allowed_sum > 0.0 and sum != compare_sum:
        raise CommandAbort("The probabilities of parameter '%s' should sum up to %s. Now the sum is %s" % (param_name, allowed_sum, (sum/fact)))



class RunBatch(DaemonCommand):

    PARAMETERS_START = "=== PARAMETERS START ==="
    PARAMETERS_END = "=== PARAMETERS END ==="

    VALIDATORS = {
        "path_exists": lambda file: os.path.exists(file),
        "probability_distribution": lambda values: sum(values) == 1.0,
    }


    description = "Run a batch of experiments from a batch file or from a dir containing multiple batch files"

    # TODO custom paths
    PATH_GENERATOR = "../prismgen/prismgen.py"
    PATH_PRISM = subprocess.check_output("which prism", shell=True).strip()

    PRISM_FLAGS = "-javamaxmem 20g -cuddmaxmem 20g -extrareachinfo -maxiters 100000"

    def __init__(self, *args, **kwargs):

        super(RunBatch, self).__init__(*args, **kwargs)
        self.ctx_decimal = getcontext()
        self.model_counter = 0
        self.spec_counter = 0


    @staticmethod
    def write_parameters(stream, params):
    
#        assert isinstance(stream, file) # not compatible with python3
        assert not stream.closed
        assert isinstance(params, dict), "Expected dictionary. Passed: %s" % params
    
        parameters_string = RunBatch.PARAMETERS_START + "\n" + \
                    yaml.dump(params, default_flow_style=True) + "\n" + \
                    RunBatch.PARAMETERS_END + "\n"
        stream.write(parameters_string)
        stream.flush()
    
    def run_batch_dir(self, batch_dir, workdir):

        dry_mode = self.get_arg("dry")
    
        batch_files = glob.glob(os.path.join(batch_dir, "*.batch"))
     
        if len(batch_files) == 0:
            self.logger.warning("Found no batch file to run in '%s' ..." % batch_dir)
    
        batch_files.sort()

        force = self.get_arg("force")
    
        for curr_batch_file in batch_files:

            if os.path.isfile(curr_batch_file):
                self.run_batch_file(curr_batch_file, workdir)
            else:
                self.logger.warning("Expected file matching '*.batch' pattern, found directory. Skip ...")


    def load_batch_parameters(self, batch_file):
        """
        This method loads the batch definitions from file. It validates the list
        of passed parameters and return a list of tuples. Each tuple contains the
        parameters of a single batch of experiments.
        """
    
        self.logger.debug("Load batch parameters ...")
    
        batch_params_list = []
    
        with open(batch_file) as f:
            batch_file = yaml.load(f)

            print("batch file: %s" % batch_file)
            parameters_common = batch_file.get("common", {})
            parameters_batches = batch_file.get("batches", {})

            for (batch_label, curr_batch) in iter(parameters_batches.items()):
                parameters_batch = parameters_common.copy()
                
                parameters_batch.update(curr_batch)

# TODO change the validation procedure and restore it later
#                self.validate_batch_parameters(parameters_batch)

                batch_params_list.append((batch_label, parameters_batch))

        self.logger.info("Load %s batches, validation succeeded ..." % len(batch_params_list))
    
        return batch_params_list
        
    
    def run_batch_file(self, batch_file, workdir):

        workdir = os.path.join(workdir, os.path.basename(batch_file) + ".out")

        dry_mode = self.get_arg("dry")

        try:
            if not dry_mode:
                os.mkdir(workdir)
            else:
                self.logger.info("Skip creating workdir '%s' because in validate mode" % workdir)
        except OSError:
            if not self.get_arg("force"):
                raise CommandAbort("The working directory '%s' already exists. Either deleted it, use the -f/--force option, or specify a different one" % workdir)

    
        self.logger.info("Load batch file: '%s'. Workdir: '%s'." % (batch_file, workdir))
    
        batch_params_list = self.load_batch_parameters(batch_file)
    
        labels = self.get_arg("labels")
    
        if not dry_mode:
    
            for (curr_label, batch_params) in batch_params_list:
                
                if not labels or curr_label in labels:
                    self.logger.info("Execute batch: %s" % (batch_params,))
                    self.run_batch(curr_label, workdir, batch_params)
    

    def read_param(self, param_spec):

        value = None
        desc = None

        if param_spec is not dict:
            value = param_spec
        else:
            value = param_spec["value"]
            desc = param_spec["desc"]

            validators = param_spec.get("check", [])

            for validator_name in validators:

                if not validator_name in self.VALIDATORS:
                    logger.warning("Validation '%s' does not exist. Go to next validation ..." % validator_name)
                    continue

                fun_validator = self.VALIDATORS[validator_name] 

                if not fun_validator(value):
                    raise CommandAbort("Validator '%s' failed on value '%s'" % (validator_name, value))

    
    def run_batch(self, label, workdir, params):

        assert isinstance(params, dict)
        assert isinstance(self.ctx_decimal, Context)
 
        # TODO force label to be [a-zA-Z_]
        if "." in label or " " in label:
            self.logger.warning("The label '%s' cannot contain neither dots nor spaces. Skip ..." % label)
            return
        
    
        # check timeout syntax
        timeout = params.get("timeout", None)
        if timeout:
            m_timeout = None
            m_timeout = re.match("([0-9]+)([dhms]?)", params["timeout"])
        else:
            self.logger.warning("Expected timeout as: <num> U, where [num] ~ [0-9]+, and U is (h)ours, (m)inutes, (s)econds or (d)ays. Skip ...")
            return


        params_ordering_model, params_ordering_spec = self.get_params_ordering(params)
        self.logger.debug("Parameter ordering before iteration. Model: %s. Spec: %s" % (params_ordering_model, params_ordering_spec))

        # split parameters according to their type [model, spec]; NB a parameter can have both types
        params_model = {}
        params_spec = {}
        for name,value in iter(params.items()):
            if name in params_ordering_model:
                params_model[name] = value
            if name in params_ordering_spec:
                params_spec[name] = value
        
        interpreter = asteval.Interpreter()
#        interpreter = SmartInterpreter()

        import_objects_list = params.get("imports", [])

        self.logger.debug("Import objects list: %s" % import_objects_list)
        self.import_objects(interpreter, import_objects_list)

        self.iterate_batch(label, workdir, timeout, params_model, params_ordering_model, params_spec, params_ordering_spec, interpreter)


    def get_params_ordering(self, params):
        
        ordering_model = deque()
        ordering_spec = deque()
        
        # remaining is treated as a queue
        remaining = deque(iter(params.items()))

        num_skipped = 0

        while len(remaining) > 0:
            # pop from the beginning of the queue
            name, attributes = remaining.popleft()
            depends = []
            usedby = ["model",]

            if name == "imports":  
                # skip the "imports" parameter since it is evaluated in a special way
                continue

            if isinstance(attributes, dict):
                depends = attributes.get("depends", [])
                usedby = attributes.get("usedby", usedby)

            satisfied = True
            for curr_dep in depends:
                self.logger.debug("Parameter: %s. Depends from: %s. Ordering model: %s. Ordering spec: %s" % (name, curr_dep, ordering_model, ordering_spec))
                if "model" in usedby and curr_dep not in ordering_model:
                    satisfied = False
                    break
                if "spec" in usedby and curr_dep not in ordering_spec:
                    satisfied = False
                    break

            self.logger.debug("Parameter: %s. Dependencies: %s. Satisfied? %s" % (name, depends, satisfied))
            if satisfied:
                if "model" in usedby:            
                    ordering_model.append(name)
                if "spec" in usedby:
                    ordering_spec.append(name)

                num_skipped = 0
            else:

                num_skipped = num_skipped + 1
                self.logger.debug("Skipped parameter for unsatisfied dependencies: %s. Dependencies: %s. Attempts: %s" % (name, depends, num_skipped))
                if num_skipped > len(remaining):
                    raise ValueError("Circular dependency in your parameters? Circular parameters: %s. Skipped: %s. Remaining: %s." % (remaining, num_skipped, len(remaining)))

                # put at the end and check again later
                remaining.append((name, attributes))

        return ordering_model, ordering_spec

 
    def eval_parameter(self, name, par_spec, interpreter): #aeval):

        values = []

        self.logger.debug("Lookup parameter: %s -> Definition: %s" % (name, par_spec))   
        if isinstance(par_spec, basestring):
            values = [ par_spec ]
        elif isinstance(par_spec, dict) and "value" in par_spec:
            values = [ par_spec["value"] ]
        elif isinstance(par_spec, dict) and "exp" in par_spec:
#            aeval.symtable.update(**env)
            self.logger.debug("Evaluating: %s. Existing values: %s" % (par_spec["exp"], values))
#            par_value = interpreter("%s" % par_spec["exp"], show_errors=True) # interpreter was aeval
            par_value = interpret(interpreter, "%s" % par_spec["exp"])

            if par_value is None:
#                for err in interpreter.error:
#                    traceback.print_exception(*err.exc_info)

                raise CommandAbort("Parameter (%s) evaluates to None. This is an error." % name)

            self.logger.debug("Evaluated parameter: %s -> %s" % (name, par_value))
            values = [ par_value ]
        elif isinstance(par_spec, dict) and "range" in par_spec:
            self.logger.debug("Interpret range: %s" % par_spec["range"])
            range_from = par_spec["range"].get("from", None)
            range_to = par_spec["range"].get("to", None)
            range_step = par_spec["range"].get("step", None)

            values = par_spec["range"].get("enum", [])

            if not values:
#                aeval.symtable.update(**env)
#                range_from = interpreter("%s" % range_from) # was: aeval
#                range_to = interpreter("%s" % range_to) # was: aeval
#                range_step = interpreter("%s" % range_step) # was: aeval
                range_from = interpret(interpreter, "%s" % range_from)
                range_to = interpret(interpreter, "%s" % range_to)
                range_step = interpret(interpreter, "%s" % range_step)


                self.logger.debug("Range from %s to %s (step: %s)" % (range_from, range_to, range_step))
#                values = xrange(range_from, range_to+1, range_step)
                values = []

                if range_to >= range_from:
                    curr = range_from
                    while curr <= range_to:
                        values.append(curr)
                        curr += range_step
                else:
                    self.logger.warning("The range for parameter %s appear to be empty: from=%s, to=%s, step=%s" % (name, range_from, range_to, range_step))
        else:
            raise ValueError("Parameter not supported: %s -> %s" % (name, par_spec))
 
        return values

    def import_objects(self, interpreter, imports):

        self.logger.debug("Imported objects: %s" % imports)

        for curr in imports:
            assert "name" in curr
            object_name = curr["name"]
            object_alias = curr.get("alias", object_name)
            object_path = curr.get("path", None)

            if object_path is None:
                new_module = pydoc.locate(object_name)
            else:
                # in this case we are importing a module
                if not os.path.exists(object_path):
                    raise ValueError("Cannot open module at path: %s" % object_path)
    
                new_module = imp.load_source(object_name, object_path)

            interpreter.symtable[object_alias] = new_module

    def iterate_batch(self, label, workdir, timeout, params_model, params_ordering_model, params_spec, params_ordering_spec, interpreter, env_model=None):

        if env_model is None:
            env_model = {} 

        if len(params_model) > 0:
            self.logger.debug("Parameter for model passed (%s): %s" % (len(params_model), params_model))
            iter_params_model = params_model.copy()
            iter_params_ordering_model = deque(list(params_ordering_model)) # copy the parameters in the queue, respecting their ordering

            self.logger.debug("Iterate parameter ordering for model (%s): %s" % (len(iter_params_ordering_model), iter_params_ordering_model))
#            while len(iter_params_ordering_model) > 0:
# TODO the while loop here is probably a bug; test the code without it
            name = iter_params_ordering_model.popleft()
            par_spec = iter_params_model.pop(name)
         
            #aeval.symtable.update(**env_model)
            values = self.eval_parameter(name, par_spec, interpreter) #aeval)
            len_values = len(values)
            for num,v in enumerate(values):
                env_model[name] = v
                interpreter.symtable[name] = v
 
                try:
                    self.logger.debug("Ready to iterate parameter %s (%s of %s) ..." % (name, num+1, len_values))
                    self.iterate_batch(label, workdir, timeout, iter_params_model, iter_params_ordering_model, params_spec, params_ordering_spec, interpreter, env_model)
                except BatchTimeoutException as e:
                    self.logger.warning("Iteration on parameter '%s' terminated because of timeout. Continue iterating on other parameters (if any). Details: %s." % (name, e))
                    break
#
        else:
            # do the job

            str_params = str(env_model)
            if len(str_params) > 500:
                str_params = "%s ... (truncated)" % str_params[:500]
            self.logger.info("Build model (%s): %s" % (label, str_params))

            assert "template" in env_model
            template_model = env_model["template"]
    
            model = self.create_model(label, workdir, template_model, env_model)

            self.logger.info("Check model (%s:%s): Parameters: %s. Ordering: %s" % (label, model, params_spec, params_ordering_spec))
            
            # some parameters may be present in both the model and the specification; in this case,
            # env_model already contain the value of such parameters, thus we need to reuse such
            # value also in the environemnt of the specification (env_spec_init), and next we can
            # remove the parameter from params_ordering_spec
    
            env_spec_init = {}
            params_ordering_spec_remaining = deque()
            params_spec_remaining = {}

            for name in params_ordering_spec:
                assert name in params_spec
                if name in env_model:
                    env_spec_init[name] = env_model[name]
                else:
                    params_ordering_spec_remaining.append(name)
                    params_spec_remaining[name] = params_spec[name]

            self.logger.debug("Check model with initial spec env: %s"% env_spec_init)
            self.check_model(label, workdir, timeout, model, env_model, params_spec_remaining, params_ordering_spec_remaining, env_spec_init, interpreter)
            self.logger.debug("Verification of model ended.")

 
    @staticmethod
    def check_deps(depends, ctx):
        assert isinstance(depends, list)
        assert isinstance(ctx, dict)

        all_deps = True
        for curr_dep in depends:
            assert isinstance(curr_dep, basestring)

            if curr_dep not in ctx:
                all_deps = False
                break

        return all_deps


    def check_model(self, label, workdir, timeout, model, env_model, params_spec, params_ordering_spec, init_env_spec, interpreter):

        self.logger.debug("Check model with initial environment for specification: %s" % init_env_spec)
        env = [ dict(**init_env_spec) ]

        assert "specs" in env_model, env_model
        template_spec = env_model["specs"]
        self.spec_counter = self.spec_counter + 1
        spec_suffix = "%s_%s" % (self.model_counter, self.spec_counter)

        self.logger.debug("Specification parameters: %s -> %s" % (params_ordering_spec, params_spec))
        for param in params_ordering_spec:
            new_env = []
            for conf in env:
                par_spec = params_spec[param]
                #aeval = asteval.Interpreter()
                #aeval.symtable["Decimal"] = Decimal
                #aeval.symtable.update(**conf)
                interpreter.symtable.update(**conf) 
                values = self.eval_parameter(param, par_spec, interpreter) #aeval)
    
                # build a new environment, multiplying each environment for the computed
                # parameter values
                for par_value in values:    
                    new_conf = dict(**conf)
                    new_conf[param] = par_value
                    new_env.append(new_conf)
            env = new_env

        # do the job
        self.logger.info("Create specification (%s): %s" % (label, env))

        ctx = { "parameters": env }
        spec = self.create_spec(label, workdir, template_spec, ctx, spec_suffix)
        timeout = self.get_timeout(timeout, env)
##
        self.logger.debug("Start verification: %s (model) vs %s (spec) [timeout=%s sec]..." % (model, spec, timeout))

        time_before = int(time.time())
#        all_params = dict(env_model)
#        all_params.update(**env)
        # TODO multiply timeout by the length of env
        exp_res = self.run_exp(workdir, model, spec, timeout, env_model) #all_params)
        time_after = int(time.time())

        delta = time_after - time_before

        if exp_res > 0:
            self.logger.error("Error while running model checker. Expected returned value (0). Got: %s" % exp_res)
        else:
            self.logger.info("Model checker checked specifications correctly: model: %s - specification: %s (%s sec) ..." % (model, spec, delta))

    def get_timeout(self, initial_timeout, env_spec):
        assert isinstance(env_spec, list)
        assert isinstance(initial_timeout, int) or isinstance(initial_timeout, basestring)

        res = -1

        if isinstance(initial_timeout, int):
            res = int * len(env_spec)
        else:
            # initial_timeout is a string

            multipliers = { "d": 3600*24, "h": 3600, "m": 60, "s": 1 }
            # initial_timeout is a string
            m = re.match(r"^([0-9]*)([dhms]?)", initial_timeout)
            if not m:
                raise ValueError("Expected a string like: <123> [d|h|m|s]. Passed: %s" % initial_timeout)
            num, unit = m.group(1), m.group(2)
            if unit not in multipliers:
                raise ValueError("Expected a timeout unit of time among the following: %s. Passed: %s" % (multipliers.keys(), unit))

            res = int(num) * multipliers[unit] * len(env_spec)

        return res
        


    def create_spec(self, label, workdir, template, ctx, spec_suffix):

        out_specification_name = "%s_%s_%s" % (label, os.path.splitext(os.path.basename(template))[0], spec_suffix)
        out_specification_path = os.path.join(workdir, out_specification_name)

        template_path = os.path.dirname(template) or "."
        template_name = os.path.basename(template)

        if os.path.exists(out_specification_path) and not self.get_arg("force"):
            raise CommandAbort("The output specification file already exists '%s'. Either delete it or retry using the -f/--force flag" % out_specification_path)

        if not os.path.exists(template_path):
            msg = "The template path '%s' must be a valid directory" % template_path
            raise CommandAbort(msg)

        t_loader = jinja2.FileSystemLoader(searchpath=template_path)
        t_env = jinja2.Environment(loader=t_loader)

        t = t_env.get_template(template_name)

        try:
            specification = t.render(ctx)
        except jinja2.exceptions.UndefinedError as e:
            raise CommandAbort("Some parameter in the specification template has not been assigned a value: %s" % e)

        try:
            with open(out_specification_path, "w+") as out_file:
                self.logger.debug("Generate specification: %s" % out_specification_path)
                out_file.write(specification)
        except Exception as e:
            raise CommandAbort("Error writing specification file: %s. Details: %s" % (out_specification_path, e))

        return out_specification_path


    def create_model(self, label, workdir, template, ctx):
 
        assert isinstance(ctx, dict)
    
        # TODO in this way we generate (almost) random names, that in the 
        # folder are not sortable by their filenames; on the other side it is 
        # not clear how to choose what parameters should end in the file name, and
        # what not. Perhaps a good tradeoff is to put all constant parameters
        # (since the others are obtained from them)

        self.model_counter = self.model_counter + 1
        out_model_name = label + "_" + os.path.splitext(os.path.basename(template))[0] + "_" + str(self.model_counter)
        out_model = os.path.join(workdir, out_model_name)

        cmd = self.registry.get("create_model")

        if not cmd:
            raise CommandAbort("Cannot find plugin 'create_model'.")

        cmd.run(template=template, out_model=out_model, ctx=ctx)

        return out_model

    
    def run_exp(self, workdir, model, spec, timeout, params):

        assert isinstance(timeout, int), "Expected integer timeout. Passed: %s" % timeout
        assert timeout > 0, "Assert positive timeout. Passed: %s" % timeout

        if not os.path.exists(spec):
            raise Exception("Spec file does not exist: %s. Abort experiment ..." % spec)
    
        niceness = self.get_arg("nice")

        prism_flags = self.PRISM_FLAGS % params
        if self.get_arg("exact"):
            prism_flags = "%s -exact" % prism_flags

        nice_flags = "-n %s" % niceness
        cli = "nice %(nice_flags)s %(command)s %(flags)s %(model)s %(spec)s" % { "command": self.PATH_PRISM, "flags": prism_flags, "model": model, "spec": spec, "nice_flags": nice_flags }
    
    
        self.logger.info("cli = %s" % cli)

        out_filename = "%s_%s.out" % (model, os.path.basename(spec),)
        err_filename = "%s_%s.err" % (model, os.path.basename(spec),)
        with open(out_filename, "w") as out_file:

            RunBatch.write_parameters(out_file, params)

            with open(err_filename, "w") as err_file:
                try:
                    exp_res = subprocess.call(cli, shell=True, stdout=out_file, stderr=err_file, timeout=timeout)
                except subprocess.TimeoutExpired as e:
                    msg = "Timeout exception (model: %s, spec: %s, timeout: %s, exception: %s)." %  (model, spec, timeout, e)
                    self.logger.error(msg)
                    raise BatchTimeoutException(msg)

    
                # if err file empty, delete it
                if os.path.exists(err_filename) and \
                        os.stat(err_filename).st_size == 0:
                    os.remove(err_filename)
    
                return exp_res
    
        return -1

    def get_args_parser(self):

        parser = super(RunBatch, self).get_args_parser()

        parser.add_argument("batch", type=str, help="the path of the batch specification file (or a directory containing *.batch files)")
        parser.add_argument("workdir", type=str, nargs="?", default="", help="the path where the results should be collected")
        
        parser.add_argument("-n", "--nice", type=bounded_int(min=-20,max=20), default=20, help="the niceness in an interval of [-20,20] (default: 20)")
        
#        parser.add_argument("-ns", "--n_step", type=int, default=10, help="the step increase for parameter N in the batch execution (default: 10)")

#        parser.add_argument("-ls", "--l_step", type=int, default=1, help="the step increase for parameter L in the batch execution (default: 1)")
        
        parser.add_argument("--dry", action="store_true", help="check only that the batch file/s is/are correct (validation mode)")

        parser.add_argument("--exact", action="store_true", help="run model checker using exact arithmetic (i.e. no float approximations)")

        parser.add_argument("-l", "--labels", default="", type=str, help="specify the batch labels to run (default: all)")

        return parser


    def run(self):

        batch_path = self.get_arg("batch").rstrip(os.sep) # remove the directory separator from right end of input
        workdir = self.get_arg("workdir") 

        if not workdir:
            workdir_parent = batch_path

            if os.path.isfile(batch_path):
                workdir_parent = os.path.dirname(batch_path)
    
            workdir = os.path.join(workdir_parent, "out")
        
        niceness = self.get_arg("nice")
        dry_mode = self.get_arg("dry")
        
        self.logger.info("Start batch: prism generator path='%s', prism path='%s', nicencess='%s', batch path='%s', workdir='%s'" % (self.PATH_GENERATOR, self.PATH_PRISM, niceness, batch_path, workdir))
        
        self.logger.debug("Found prism path: %s ..." % self.PATH_PRISM)
        
        if not os.path.exists(batch_path):
            raise CommandAbort("The batch path '%s' does not exist" % batch_path)
        
        try:
            if not dry_mode:
                os.makedirs(workdir)   
            else:
                self.logger.info("Skip creating workdir '%s' because in validate mode ..." % workdir)
        except OSError as error:
            if error.errno != errno.EEXIST:
                raise
            else:
                self.logger.debug("Skip creating workdir '%s' as it already exists ..." % workdir)

        
        if os.path.isfile(batch_path):
            self.run_batch_file(batch_path, workdir)
        else:
            self.run_batch_dir(batch_path, workdir)
