import os
import sys
import argparse
import shlex
import subprocess
import abc

from secmc.plugins import CommandAbort, DaemonCommand

# tweak with prompt_toolkit
import prompt_toolkit as pt
from pygments.token import Token

class PromptInterrupt(CommandAbort):
    pass


class Interface(object):

    __metaclass__ = abc.ABCMeta

    COMMANDS = [ "exit", "quit", "help" ]

    def __init__(self, registry, logger):
    
        self.registry = registry
        self.logger = logger

    @abc.abstractmethod
    def read_line(self):
        return None


    def loop(self):

        while True:

#            print "loop ..."
            try:
                line = self.read_line()
#                print "read line: %s ..." % line

                if line is None:
                    break
                elif len(line) > 0:
#                    print "before"
                    self.interpret(line)    
#                    print "after"
                else:
                    self.do_help()     
            except (EOFError, PromptInterrupt):
#                print "exception 1"
                break
            except (KeyboardInterrupt, CommandAbort):
#                print "exception 2"
                continue

    def interpret(self, line):

        if line.startswith("#"):
            # it's just a comment
            return
        elif line.startswith("!"):
            # shell command
            self.run_shell(line)
        elif line in self.COMMANDS:
#            print "run command ..."
            self.run_command(line)
        else:
#            print "run tool ..."
            self.run_tool(line)

    def run_shell(self, line):
     
        shell_command = line[1:].strip()
  
        tokens = shlex.split(shell_command)
        # something does not work here (e.g. the command "sleep 10" or "echo ciao"
        # don't work properly)
        subprocess.call(tokens, shell=True)
#        subprocess.call(line, shell=False)


    def run_command(self, line):
        assert line in self.COMMANDS

        method_name = "do_%s" % line

        method = getattr(self, method_name)
        method(line)


    def run_tool(self, line):

        cli_args = line.split(" ")

        cli_args = map(lambda a: a.strip(), cli_args)

        tool_name = cli_args[0]

        if self.registry.get(tool_name):
#            print "before execute registry ..."
            self.registry.execute(cli_args)
#            print "after execute registry ..."
        else:
            self.logger.error("Command '%s' is not recognized. Print help for a list of available tools and commands." % tool_name)



 

    def do_exit(self, line):
        raise PromptInterrupt()

    def do_quit(self, line):
        raise PromptInterrupt()

    def do_help(self, line=""):

        command_names = sorted(self.COMMANDS)
        tool_names = sorted(self.registry.get_names())
 
        print("Commands: %s" % (",".join(command_names,)))
        print("Tools: %s" % (",".join(tool_names,)))
     


class CLI(Interface):

    PROMPT = u"secmc:> "

    PROMPT_STYLE = pt.styles.style_from_dict({
            # User input.
            Token:          '#ff0066',
        
            # Prompt.
#            Token.Username: '#884444',
#            Token.At:       '#00aa00',
#            Token.Colon:    '#00aa00',
            Token.Tool:     '#00aa00',
            Token.Pound:    '#00aa00',
#            Token.Host:     '#000088 bg:#aaaaff',
#            Token.Path:     '#884444 underline',
        })

    @staticmethod
    def get_prompt_tokens(cli):
        
        return [
            (Token.Tool,     'secmc'),
            (Token.Pound,    ':> '),
        ]
    
    def __init__(self, *args, **kwargs):
    
        self.history = pt.history.InMemoryHistory()
        super(CLI, self).__init__(*args, **kwargs)
    
    def read_line(self):
        """
        Read a line from the prompt and return it.
        """
        line = pt.prompt(self.PROMPT, 
            history=self.history,
#            get_prompt_tokens=CLI.get_prompt_tokens,
            style=CLI.PROMPT_STYLE)

        return line


class Script(Interface):

    def __init__(self, script_path, *args, **kwargs):
        self.script_path = None
        self.script_in = open(script_path, "r")

        super(Script, self).__init__(*args, **kwargs)

    def read_line(self):
        """
        Read a new line from the passed file. If EOF is reached, return None.
        """
        if not self.script_in:
            raise Exception("Cannot read_line without opening a file")

        orig = self.script_in.readline()
        line = orig.strip()

        while (orig != '' and not line):
            orig = self.script_in.readline()
            line = orig.strip()

        if orig == '':  
            line = None

        return line

class Prompt(DaemonCommand):

    description = "Start a prompt for interpreting commands from the user"


    def get_args_parser(self):
    
        parser = super(Prompt, self).get_args_parser()

        parser.add_argument("-s", "--script", type=str, help="the path of a script to be executed")

        return parser

    def run(self):
 
        script_path = self.get_arg("script")

        interface = None

        if script_path:
            interface = Script(script_path, self.registry, self.logger)
        else:
            interface = CLI(self.registry, self.logger)

        assert interface is not None

        interface.loop()

