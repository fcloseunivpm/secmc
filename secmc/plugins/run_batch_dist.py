import socket
import multiprocessing
import subprocess
import dispy 
import re
import os
import netifaces
import shutil
import tempfile
import time

from secmc.plugins import CommandAbort
from secmc.plugins.run_batch import RunBatch

import logging
import yaml # required to run sec.plugins.run_batch

interfaces = {}
LOCAL_ADDRESSES = {}


for i in netifaces.interfaces():

    interfaces[i] = netifaces.ifaddresses(i)
    
for k,v in interfaces.items():
        
    if netifaces.AF_INET in v.keys():
        
        LOCAL_ADDRESSES[k] = v[netifaces.AF_INET][0]['addr']


def is_valid(node):
    
    if re.match('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$', node) or re.match('^[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$', node):
        return True
    else:
        return False


                 
def run_job(submit_params, node):
    
    assert len(submit_params) == 8
    
    (timeout, prism_flags, model, specs, nice_flags, out_filename, err_filename, params) = submit_params

    temp_dir = os.path.join(tempfile.gettempdir(), 'dispy', 'node', node)
    
    if os.name == 'posix':
    
        job_folder = subprocess.check_output("ls %s" % temp_dir, shell=True).strip()
        
        #path_prism = subprocess.check_output("which prism", shell=True).strip()
        path_prism = os.path.expanduser("~/bin/prism")
        
    
    elif os.name == 'nt':
    
        job_folder = subprocess.check_output("dir /B %s" % temp_dir, shell=True).strip()
        
        path_prism = subprocess.check_output("where prism", shell=True).strip()
    
    
    temp_file_path = os.path.join(temp_dir, job_folder)


    cli = "timeout %(timeout)s nice %(nice_flags)s %(command)s %(flags)s %(model)s %(specs)s" % { "timeout": timeout, "command": path_prism, "flags": prism_flags, "model": os.path.join(temp_file_path, model), "specs": os.path.join(temp_file_path, specs), "nice_flags": nice_flags }   
        
    out_filename = os.path.join(temp_file_path, out_filename) 
    err_filename = os.path.join(temp_file_path, err_filename) 
        
    job_res = -1
    try:
        with open(out_filename, 'w+') as out_file:
            with open(err_filename, 'w') as err_file:
                exp_res = subprocess.call(cli, shell=True, stdout=out_file, stderr=err_file)
            
        # if err file not empty, send it back (from /tmp/dispy/node...)
        if os.path.exists(err_filename) and os.stat(err_filename).st_size != 0:
            dispy_send_file(err_filename)
    
        dispy_send_file(out_filename)
        job_res = exp_res
    except Exception as e:
        job_res = -1
    
    return job_res




 
        
class RunBatchDist(RunBatch):

    _number_cpus= multiprocessing.cpu_count()

    DISPYNODE_COMMAND = "dispynode.py"
    
    IFACES = { "eth0": "enp0s31f6", "wlan0": "wlp3s0" }
    
    def _get_iface_addr(self, iface):
        
        iface = self.IFACES.get(iface, iface)

        addr = LOCAL_ADDRESSES.get(iface, None)

        return str(addr)

    def __init__(self, *args, **kwargs):
        super(RunBatchDist, self).__init__(*args, **kwargs)
        
        self.NODES = []
        
        self.cluster = None
        
        self.jobs = []
        
        self.dict_cli = {}
        
        self.job_id = 1
        
        # self.depends contains the path to files that should be submitted to all nodes
        self.depends = set([])
        
        self.node = None
        
        #self.ip_addr = []
        self.IP_ADDRESS = None
        
        self.http_server = None
        
        self.http_server_dir = None
        
        
    def get_args_parser(self):
        """
        TODO this seems to work only if launch the command as:
        
        secmc run_batch_dispy file.batch folder --nodes XXXX:YY
        
        It does not work if you launch the command as:
        
        secmc run_batch_dispy --nodes XXXX:YY file.batch folder

        But it should work ...        
        """
    
        parser = super(RunBatchDist, self).get_args_parser()
        
        default_address = self._get_iface_addr("lo") #self._get_iface_addr("eth0") or self._get_iface_addr("wlan0") or self._get_iface_addr("lo")

        parser.add_argument("--nodes", type=str, nargs='*', default=[default_address], help="list the nodes where the computation should be executed as IPv4, IPv6 or host name: <local_ip_address> | <host_name>, eg: ubuntu, 192.168.100.200. (default: %s)" % default_address)

        parser.add_argument("--ip_address", type=str, nargs='*', default=default_address, help="IP address to use when sending jobs to the network (default: %s)" % default_address)
        
        parser.add_argument('--http', type=int, default=0, help="Set the port through which connet to http Server")
        
        return parser

   
    def run_exp(self, workdir, model, specs, timeout, params):  
                
        
        if not os.path.exists(specs):
            raise Exception("Spec file does not exist: %s. Abort experiment ..." % specs)
    
        niceness = self.get_arg("nice")

        prism_flags = "%s" % self.PRISM_FLAGS
        if self.get_arg("exact"):
            prism_flags = "%s -exact" % prism_flags

        nice_flags = "-n %s" % niceness
        
        out_filename = "%s_%s.out" % (model, os.path.basename(specs),)
        temp_out_filename = "%s.temp" % out_filename
        err_filename = "%s_%s.err" % (model, os.path.basename(specs),) 
        
        self.depends.add(specs)
        
        job_params =  [ timeout, prism_flags, model, specs, nice_flags, temp_out_filename, err_filename, params, ]
        job_depends = [ model, specs,  ]
        
        self.dict_cli[self.job_id] = { "params": job_params, "depends": job_depends }
        
        
        assert not os.path.exists(out_filename), "Output filename should not exist, already: %s" % out_filename
        with open(out_filename, "w+") as out_file:
            RunBatch.write_parameters(out_file, params)


        self.job_id = self.job_id + 1
        

    def launch_jobs(self):
        
        for ip in self.NODES:
            for curr in self.depends:
                # TODO prova a spedire l'intera lista di file con un metodo solo dell'api dispy
                self.cluster.send_file(curr, ip)
        
        for k, v in iter(self.dict_cli.items()):
             
            assert isinstance(v, dict)
            assert "params" in v
            assert "depends" in v
            
            job = self.cluster.submit(v["params"], self.node, dispy_job_depends=v["depends"])  
            
            self.jobs.append(job)
            
        ok_jobs = set([])
        err_jobs = set([])    
        for job in self.jobs:
            
            job()
            
            job_id = self.jobs.index(job)+1
            self.logger.info("Returned job %s:\t exit code(%s)" % (job_id, job.result))
            if job.result is None:
                self.logger.error("Exception in job %s: %s" % (job_id, job.exception.message))
                self.logger.exception(job.exception)
                err_jobs.add(job_id)
            elif int(job.result) > 0:
                self.logger.error("Error in job %s: %s" % (job_id, job.stderr))
                err_jobs.add(job_id)
            else:
                # job.result == 0
                ok_jobs.add(job_id)
            
        self.logger.info("All jobs returned. Correct: %s. With errors: %s" % (ok_jobs, err_jobs))
        
        for job_id in ok_jobs:
            job_params = self.dict_cli[job_id]["params"]
            assert len(job_params) > 7, job_params
            temp_out_filename = job_params[5]
            model_check_params = job_params[7]
            assert temp_out_filename.endswith(".temp"), "Expected file ending with .temp. Passed: %s" % temp_out_filename
            out_filename = re.sub("\.temp$", "", temp_out_filename) # remove the .temp extension
     
            assert os.path.exists(temp_out_filename), "File not found (%s)" % (temp_out_filename,)
            assert os.path.exists(out_filename), "Output file should exist already (%s)" % (out_filename,)
            with open(out_filename, "a") as out_file:
                with open(temp_out_filename, "r") as temp_out_file:
                    # append the job output to the parameters, in the output file
                    out_file.write(temp_out_file.read())

            os.unlink(temp_out_filename)
    

    def initialize(self):
    
        super(RunBatchDist, self).initialize()
        
        self.NODES = self.get_arg("nodes")
        self.IP_ADDRESS = self.get_arg("ip_address")
        
        for node in self.NODES:
            if not is_valid(node): 
                raise CommandAbort("Node %s is not valid, <{ip_addess}^{remote_node}> needed..." % node)       
        
        #self.NODES.insert(0, '*')  Con '*' vengono automaticamente rilevati tutti i nodi con dispynode attivi nella LAN del client
        
        #self.node = LOCAL_ADDRESSES['wlan0']
        default_address = self._get_iface_addr("lo") #self._get_iface_addr("eth0") or self._get_iface_addr("wlan0") or self._get_iface_addr("lo")
        self.node = default_address

        self.logger.info("Submit job to nodes %s using IP %s" % (self.NODES, self.IP_ADDRESS,))
        self.cluster = dispy.JobCluster(run_job, nodes=self.NODES, ip_addr=self.IP_ADDRESS) #, ext_ip_addr="")              
    
       
      
      
        
        
    def run(self):
    
        if self.get_arg("http") > 0:
#            import httpd    #Import error. Dopo aver copiato il file httpd.py in /usr/local/bin non da` piu` errore.
            import dispy
            
            port = self.get_arg("http")
            
            self.http_server_dir = os.path.join(tempfile.gettempdir(), 'dispy', 'node', 'DispyHTTPServer')
            
            shutil.copytree(os.path.expanduser("~/.local/lib/python2.7/site-packages/dispy/data"), dst=self.http_server_dir)    
            
            self.http_server = dispy.httpd.DispyHTTPServer(self.cluster, port=port, host="127.0.0.1", poll_sec=5, \
                DocumentRoot=self.http_server_dir)
        
        super(RunBatchDist, self).run()
    
        self.launch_jobs()
        
    
    def cleanup(self):
        
        
        self.cluster.wait()
        self.cluster.print_status()
 
        if self.http_server is not None:       
#        if isinstance(self.http_server, httpd.DispyHTTPServer):
            self.http_server.shutdown(wait=True)
            shutil.rmtree(self.http_server_dir)
            
        self.cluster.close()
