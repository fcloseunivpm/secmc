import argparse
import logging
import daemon
import inspect
import sys
import re
import os
import errno
import lockfile
from past.builtins import basestring

def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]


class CommandAbort(Exception):
    pass


class CommandArgumentParser(argparse.ArgumentParser):

    def error(self, message):
        self.print_help()
        raise CommandAbort(message)


class CommandLogger(logging.Logger):

    LOG_LEVEL_MAP = { 
        "D": logging.DEBUG, 
        "I": logging.INFO, 
        "W": logging.WARNING, 
        "E": logging.ERROR  
    }

    @staticmethod
    def set_level(logger, level):

        assert isinstance(logger, logging.Logger)
        assert isinstance(level, basestring)
    
        log_level = logging.INFO

        if level in CommandLogger.LOG_LEVEL_MAP:
            log_level = CommandLogger.LOG_LEVEL_MAP[level]
        else:
            raise CommandAbort("The specified log level (%s) is not recognized" % level)

        logger.setLevel(log_level)

    @staticmethod
    def setup_logger(log_level="D"):

        # create logger
        logger = logging.getLogger('secmc')

        if len(logger.handlers) == 0:
            # at the first invocation we add an handler
            # (if none exists)

            # create console handler and set level to debug
            ch = logging.StreamHandler()
    
            # create formatter
            formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
    
            # add formatter to ch
            ch.setFormatter(formatter)
    
            # add ch to logger
            logger.addHandler(ch)
    
        CommandLogger.set_level(logger, log_level)

        assert isinstance(logger, logging.Logger)

        return logger



class Command(object):

    description = "Put here the description of your command"

    def __init__(self, script_name, command_name, registry):

        assert isinstance(script_name, basestring)
        assert isinstance(command_name, basestring)
        assert isinstance(registry, CommandRegistry)

        self.script_name = script_name
        self.command_name = command_name
        self.registry = registry

        self.parser = None
        self._args = {} #None

        self.logger = CommandLogger.setup_logger()



    def get_args_parser(self):

        # build the default parser
        prog = "%s %s" % (self.script_name, self.command_name)
        parser = CommandArgumentParser(description=self.description, prog=prog)
        parser.add_argument("-v", "--verbose", type=str, choices=["D","I","W","E"], help="the verbose level between: (D)ebug, (I)nfo, (W)arning, (E)rror (default: I)", default="I")
        parser.add_argument("-f", "--force", help="force overwrite output file if already exists", action="store_true")

        return parser

    def _parse_args(self, cli_args):
        """
        This methods set the current parser for arguments and parse the passed
        list of arguments.
        """

        self.parser = self.get_args_parser()
    
#        print "prima di parser.parse_args ..."
        self._args = self.parser.parse_args(cli_args) #sys.argv[2:])
#        print "dopo parser.parse_args ..."


    def get_arg(self, name):
        assert isinstance(name, basestring)
        
        if not self._args: 
            raise Exception("You must first parse the arguments")

        if not hasattr(self._args, name):
            raise Exception("Argument '%s' is not allowed" % name)

        return getattr(self._args, name)

    def execute(self, cli_flags):
    
        assert isinstance(cli_flags, list)
        
#        print "in execute ..."
        self._parse_args(cli_flags)
#        print "... dopo parse ..."
        verbose = self.get_arg("verbose")
        self.logger = CommandLogger.setup_logger(verbose)
    
        self.initialize()

        run_exception = None

        try:
#            print "before run ..."
            self.run()
#            print "after run ..."
        except Exception as e:
            # postpone re-raising exception 
            # after the cleanup() operation
#            print "excpetion 3"
            self.logger.exception(e)
            run_exception = e

        self.cleanup()

        if run_exception:
            raise CommandAbort("Error during execution: %s" % run_exception)

    def initialize(self):
        """
        Do something here to prepare the context of the run() method
        """
        pass
    
    def run(self):
        """
        This should contain the main task of your plugin
        """
        pass

    def cleanup(self):
        """
        Do something in case you have to cleanup the context after the 
        plugin execution
        """
        pass


class DaemonCommand(Command):

    DIR_RUN = "/var/tmp/secmc/run/"

    def __init__(self, *args, **kwargs):

        # this will store the DaemonContext
        self.dc = None

        super(DaemonCommand, self).__init__(*args, **kwargs)
        

    def get_args_parser(self):

        parser = super(DaemonCommand, self).get_args_parser()

        parser.add_argument("-d", "--daemon", action="store_true", help="run the program in background, detached from current terminal; log everything to file")

        return parser


    def _check_or_create_aux_dirs(self):
        """
        Test and create (atomically) required directories for running 
        a daemon command   
        """

        required_dirs = [ self.DIR_RUN, ]

        for curr_dir in required_dirs:
            try:
                os.makedirs(curr_dir)
            except OSError as exception:
                if exception.errno != errno.EEXIST:
                    raise


    def initialize(self):

        is_daemon = self.get_arg("daemon")
        
        if is_daemon:
 
            assert self.dc is None       

            self._check_or_create_aux_dirs()

            lockfile_path = "%s/%s_%s.pid" % (self.DIR_RUN, self.script_name, self.command_name)
    
            self.dc = daemon.DaemonContext(
                detach_process = True,
                pidfile = lockfile.FileLock(lockfile_path),
                stdout = sys.stdout,
                stderr = sys.stderr,
                working_directory = os.getcwd()
            )

            self.dc.open()

            import time
            time.sleep(100)

            self.logger.info("Switched to daemon mode (cwd=%s, lock file=%s)..." % (os.getcwd(),lockfile_path))
            

    def cleanup(self):

        is_daemon = self.get_arg("daemon")

        if is_daemon:
            
            assert self.dc is not None

            self.logger.info("I'm about to close the daemon...")
            self.dc.close()


class CommandRegistry(object):

    def __init__(self):
        self._registry = {}
        self.logger = CommandLogger.setup_logger()
        self.command_default = None
        self.parser = None

    def add(self, name, command_class, is_default=False):
        assert isinstance(name, basestring)
        assert inspect.isclass(command_class)


        if name in self._registry:
            raise Exception("Command '%s' has already been registered" % name)

        if is_default:

            if self.command_default != None:
                raise Exception("Cannot set '%s' as default command, there already exists a default command")
    
            self.command_default = name

        command = command_class(script_name=sys.argv[0], command_name=name, registry=self)
        assert isinstance(command, Command)
        # TODO the assertion should be done at the beginning,
        # checking that command_class extends Command
        # but it's not so easy (apparently) ...

        self._registry[name] = command


    def get(self, name):
        assert isinstance(name, basestring), "Passed: %s" % name

        cmd = None

        if name in self._registry:
            cmd = self._registry[name]

            assert isinstance(cmd, Command)

        return cmd


    def get_names(self):
        return self._registry.keys()


    def get_args_parser(self):
        """
        This method returns the arguments passed through the command line
        """
        
        main_parser = CommandArgumentParser(description="Run one tool of the suite for model checking security properties")

        command_names = sorted(self._registry.keys())

        nargs = "+"
        if self.command_default is not None:
            nargs = "?"

        main_parser.add_argument("command", choices=command_names, nargs=nargs, default=self.command_default, type=str, help="name of the command to execute")

        return main_parser

    def get_args(self, cli_args):
        """
        This method set the current argument parser, and parse the passed 
        group of arguments.
        """
    
        self.parser = self.get_args_parser()

        args = self.parser.parse_args(cli_args[0:1]) #sys.argv[1:2])    

        return args


    def execute(self, cli_flags=None):
    
        if not cli_flags:
            # if no arg is passed, parse them from sys.argv
            cli_flags = sys.argv[1:]

        try:
            args = self.get_args(cli_flags)

            cmd = self.get(args.command)

#            print "before execute ..."
            cmd.execute(cli_flags=cli_flags[1:])
#            print "after execute ..."
        except CommandAbort as e:
            self.logger.error("Command exit: %s" % e)
        except Exception as e:
            print "new exception (%s: %s) ..." % (type(e), e)
            
