import dispy
import socket
import re
import netifaces

from secmc.plugins import Command, CommandAbort



def job_killer():
    return 0
    

#executed as cleanup function of dispy.JobCluster: it shuts down all dispynode previously activated
def shutdown():
    dispynode_shutdown()


class StopScheduler(Command):

 
    def __init__(self, *args, **kwargs):

        super(StopScheduler, self).__init__(*args, **kwargs)

        interfaces = {}
        self.local_addresses = {}
        
        
        for i in netifaces.interfaces():
        
            interfaces[i] = netifaces.ifaddresses(i)
            
        for k,v in interfaces.items():
                
            if netifaces.AF_INET in v.keys():
                
                self.local_addresses[k] = v[netifaces.AF_INET][0]['addr']
 
       
    def get_args_parser(self):
        
        parser = super(StopScheduler, self).get_args_parser()
        
        default_nodes = []
        if "lo" in self.local_addresses:
            default_nodes.append(self.local_addresses["lo"])
        default_nodes_argument = " ".join(default_nodes)

        parser.add_argument("--nodes", type=str, nargs='*', default=default_nodes_argument, help="Close the dispynode process in the specified nodes: <ip_address> | <user>@<device_name | host_name>, eg: 192.168.100.200, ubuntu@192.168.100.200. (default: %s)" % default_nodes_argument)
        
        parser.add_argument('--hard', action='store_true', default=False, help="")
        
        
        return parser
        
    
    def is_valid(self, node):
    
        if re.match('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$', node) or re.match('^[a-zA-Z0-9._-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$', node):
            return True
        else:
            return False
        
   
    
    def kill_hard(self): 
        pass
        
        
    def kill_soft(self):
        
        nodes = self.get_arg('nodes')
        if isinstance(nodes, str):
            nodes = [ nodes ]    

        for curr_node in nodes:
            
            if not self.is_valid(curr_node): 
                raise CommandAbort("Node %s is not valid, <{ip_addess}^{remote_node}> needed..." % node)
 
        for curr_node in nodes:
        
            cluster = dispy.JobCluster(job_killer, nodes=[curr_node], cleanup=shutdown)
            cluster.submit() 
            cluster.wait()
            cluster.close()
            
    
    
    
    
    def run(self):
        
        if self.get_arg('hard'):
            
            self.kill_hard()
            
        else:
            
            self.kill_soft()
                    
        
        
    
