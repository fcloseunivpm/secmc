import os

from secmc.plugins import Command, CommandAbort, natural_keys
import glob
import re
import yaml

from decimal import Decimal

def as_string(value):
    return u"%s" % value

class ComputeProbability(Command):
    """
    This command is responsible for parsing the PRISM model checker output files
    and collects the probabilities it returns as result of model checking
    P_min(\phi) or P_max(\phi), where \phi is a path formula in PCTL*
    """

    PARAMETERS_START = "=== PARAMETERS START ==="
    PARAMETERS_END = "=== PARAMETERS END ==="

    description = "Parse the files produced by the model checker and output a table of probabilities of attack"

    def get_args_parser(self):
        parser = super(ComputeProbability, self).get_args_parser()
       
        parser.add_argument("in_dir", type=str, help="the path of the directory storing the outcome of model checker")
        parser.add_argument("out_table", type=str, help="the path of the output table of probabilities of attack", nargs="?")

        return parser

    @staticmethod
    def get_labels(in_dir_path):
    
        files = []

        for curr_entry in os.listdir(in_dir_path):

            full_path = os.path.join(in_dir_path, curr_entry)

            if os.path.isfile(full_path) and curr_entry.endswith(".out"):
                files.append(curr_entry.split(".")[0])

        return list(set(files))

    @staticmethod
    def grep(regexp, file_path):
    
        matches = []

        pattern = re.compile(regexp)

        try:
            with open(file_path, "r") as file:
                for line in file:
                    found = pattern.findall(line)

                    if found:
                        matches.extend(found)
        except Exception as e:
            raise CommandAbort("Error reading file '%s' and execute grep on it: %s" % (file_path, e))


        return matches


    def run(self):

        in_dir_path = self.get_arg("in_dir").rstrip("/")

        (parent, child) = os.path.split(in_dir_path)
        out_table_path = self.get_arg("out_table") or child + ".csv"


        if os.path.exists(out_table_path) and not self.get_arg("force"):
#            self.logger.error("The output file '%s' already exist. Either force rewriting or delete it." % out_table_path)
#            exit(1)
            raise CommandAbort("The output file '%s' already exist. Either force rewriting or delete it." % out_table_path)

        try:
            labels = self.get_labels(in_dir_path)

            with open(out_table_path, "w+") as out_table:

#                out_table.write("#label,n,probability,k1,k2,time(sec),lenslice,l,q,n_reads\n")
                printed_headers = False

                for curr_label in labels:

                    matching_files = glob.glob("%s/%s.*.out" % (in_dir_path,curr_label))

                    matching_files.sort(key=natural_keys)

                    for in_file_path in matching_files:
    
                        # look for the probability                      
                        probabilities = self.grep("Result: [^ ]*", in_file_path)

                        if len(probabilities) == 0:
                            probabilities = [ "NN NN" ]

##                        if len(probability) > 0:
##                            probability = probability[0].split(" ")[1]
##                        else:
##                            probability = "NaN"
##
##                        # get time for model construction
##                        time_construction = self.grep("Time for model construction: [0-9\.]+", in_file_path)
##                        if len(time_construction) > 0:
##                            time_construction = Decimal(time_construction[0].split(":")[1].strip())
##                        else:
##                            time_construction = Decimal("0.0")
##
##                        # get time for model checking
##                        time_checking = self.grep("Time for model checking: [0-9\.]+", in_file_path)
##                        if len(time_checking) > 0:
##                            time_checking = Decimal(time_checking[0].split(":")[1].strip())
##                        else:
##                            time_checking = Decimal("0.0")
##
##                        # get time jacobi
##                        time_jacobi = self.grep("Jacobi: [0-9]+ iterations in [0-9\.]+", in_file_path)
##                        if len(time_jacobi) > 0:
##                            time_jacobi = Decimal(time_jacobi[0].split(" ")[4].strip())
##                        else:
##                            time_jacobi = Decimal("0.0")
##
##                        # sum partial times
##                        time = "%s" % (time_construction + time_checking + time_jacobi)
##
                        time = "???" # TODO refactor code for computing time
                        parameters = self.get_parameters(in_file_path)
                        assert isinstance(parameters, dict)

                        if not printed_headers:
                            # print the heading line only the first time
                            col_headers = [ "label", "probability", "time", "num_prob" ] + \
                                            map(as_string, parameters.keys()) 
                            out_table.write("#" + ",".join(col_headers) + "\n")
                            printed_headers = True
                           
                        for num_prob, curr_prob in enumerate(probabilities):
                            curr_prob = curr_prob.split(" ")[1]

                            row = [curr_label, curr_prob, time, str(num_prob)] + map(as_string, parameters.values())
                            self.logger.debug("Row: %s" % row)
                            out_table.write(",".join(row) + "\n")

        
        except Exception as e:
            self.logger.error("Error opening directory with model checker results or writing output table")
            self.logger.exception(e)


    def get_parameters(self, in_file_path):
        res = {}

        with open(in_file_path, "r") as in_file:
            # head: <yaml serialization of parameter list>

            parameters_string = ""
            start_parameters = False
            for line in in_file:    
                line = line.strip()
                self.logger.debug("Line: '%s'" % line)
                if line.startswith(ComputeProbability.PARAMETERS_START):
                    start_parameters = True
                elif line.startswith(ComputeProbability.PARAMETERS_END):
                    break
                elif start_parameters:
                    parameters_string = parameters_string + "\n" + line

            if len(parameters_string) == 0:
                raise ValueError("The file '%s' is supposed to contain the parameters, but they were not found" % in_file_path)

            params = {}
            try:
                params = yaml.load(parameters_string)
            except Exception as e:
                raise CommandAbort("Error parsing parameters in file: %s. Details: %s" % (in_file_path,e))

            for (name, value) in params.items():
                if not isinstance(value, dict) and not isinstance(value, list):
                    res[name] = value

        return res
            

