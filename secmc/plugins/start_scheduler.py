import socket
import subprocess
import dispy
import re
import netifaces 

from secmc.plugins import Command, CommandAbort


        
        
class StartScheduler(Command):

    DISPYNODE_COMMAND = "dispynode.py"
 
    def __init__(self, *args, **kwargs):

        super(StartScheduler, self).__init__(*args, **kwargs)

        interfaces = {}
        self.local_addresses = {}
        
        
        for i in netifaces.interfaces():
        
            interfaces[i] = netifaces.ifaddresses(i)
            
        for k,v in interfaces.items():
                
            if netifaces.AF_INET in v.keys():
                
                self.local_addresses[k] = v[netifaces.AF_INET][0]['addr']
        
 
   
    def get_args_parser(self):
        
        parser = super(StartScheduler, self).get_args_parser()
        
        default_nodes = []
        if "lo" in self.local_addresses:
            default_nodes.append(self.local_addresses["lo"])
        default_nodes_argument = " ".join(default_nodes)

        parser.add_argument("--nodes", type=str, nargs='*', default=default_nodes_argument, help="list the nodes where launch dispynode: <ip_address> | <user>@<device_name | ip_address>, eg: 192.168.100.200, ubuntu@192.168.100.200. (default: %s)" % default_nodes_argument)
        
        return parser
 

    def is_valid(self, node):
    
        if re.match('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$', node) or re.match('^[a-zA-Z0-9._-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$', node):
            return True
        else:
            return False
        
   
    
    def _launch_dispynodes(self):
        
        nodes = self.get_arg("nodes")
        if isinstance(nodes, str):
            nodes = [ nodes ]

        for curr_node in nodes:
            if not self.is_valid(curr_node): 
                raise CommandAbort("Node %s is not valid, <{ip_addess}^{remote_node}> needed..." % node)
        
        for curr_node in nodes:
        
            if curr_node not in self.local_addresses.values():
             
                try:
                    (username, device) = curr_node.split('@')
                except ValueError:
                    raise CommandAbort("Node address %s is not a local address. Please, specify a username and device to launch dispynode on a remote note" % node)
                try:
                    subprocess.Popen("ssh -p 60022 %(node)s workon fclose '&&' %(dispynode)s \
                        --ip_addr=%(device)s --daemon --clean --client_shutdown -c -2"  % \
                        {'node': curr_node, 'dispynode':self.DISPYNODE_COMMAND, 'device': device}, shell=True)
                    #    --ext_ip_addr=193.205.129.84   -R 51347:localhost:51347 
                except Exception as e:
                    raise CommandAbort("Failed to launch dispynode on the node %s: %s" % (device, e))
            
            else:
                try:
                    subprocess.Popen([self.DISPYNODE_COMMAND, "--ip_addr=%s" % curr_node, "--daemon", "--clean", "--client_shutdown", "-c -2" ])
                
                except: 
                    raise CommandAbort("Failed to launch dispynode with ip address %s" % self.local_addresses['localhost'] )
            
        return
    
    
    
    def run(self):
    
        try:   
            self._launch_dispynodes()
        except Exception as e:
            raise CommandAbort("Impossible to launch dispynode: %s" % e)
        
