import os
import jinja2
import subprocess
import re
import math

from secmc.plugins import Command, CommandAbort

class Graph(Command):

    description = "Parse the table of probabilities to build a graph image"

#    OCTAVE_TEMPLATE_PATH = "./templates/graph_log.m"
    OCTAVE_TEMPLATE_PATH = "./templates/graph_lin_multi.m"
    
    SPEC_COLOR = [ "k", "r", "g", "b", "c", "m", ]
    SPEC_LINE = [ "-", "--", ":", "-.", ]
    SPEC_MARKER = [ "+", "o", "*", ".", "x", "s", "d", "^", "v", ">", "<", "p", "h" ]

    FIG_COLS = 2


    def get_args_parser(self):
    
        parser = super(Graph, self).get_args_parser()
    
        parser.add_argument("in_table", type=str, help="the path of the table with probabilities of attack")
        parser.add_argument("out_model", type=str, help="the path of the output octave model for the graph", nargs="?")

        parser.add_argument("--x", type=str, help="the column position or name of data on X axis (default: 1)", default="1")
        parser.add_argument("--y", type=str, help="the column position or name of data on Y axis (default: 2)", default="2")
        parser.add_argument("--z", type=str, help="the column position or name of data on Z axis (default: 3)", default="2")

        # TODO the --label argument should be of same type as --x and --y
        parser.add_argument("--label", type=int, help="the column position or name of the label of the series (default: 0)", default=0)

        parser.add_argument("--silent", "-s", action="store_true", help="do not show the generated graph")

        return parser

    def parse_cols_args(self, headers):

        assert isinstance(headers, list)

        # reverse header line to a dictionary: column header -> position

        header_rev = {}

        for (id, curr_header) in enumerate(headers):

            # in the column name delete everything in parenthesis; e.g.
            # "foo(fie and fee)" becomes "foo"
            curr_header = re.sub(r'\([^\)]*\)', '', curr_header)
            curr_header = curr_header.strip(" ")

            if curr_header in header_rev:
                raise CommandAbort("The header line contains twice the same column header (%s)" % curr_header)

            header_rev[curr_header] = id

        # parse the X col specs argument

        x_pos = self.parse_col_arg(self.get_arg("x"), header_rev)

        # parse the Y cols specs argument

        y_args = self.get_arg("y").split(",")
        y_pos_list = []

        for curr_col in y_args:
            y_pos_list.append(self.parse_col_arg(curr_col, header_rev))

        z_args = self.get_arg("z").split(",")
        z_pos_list = []

        # check 3D graphs are possible only if there is a single y data column
        if len(y_pos_list) != 1:
            raise CommandAbort("In order to plot 3D graphs it is mandatory to have exactly one data column in the y axis")

        for curr_col in z_args:
            z_pos_list.append(self.parse_col_arg(curr_col, header_rev))


        return (x_pos, y_pos_list)


    def parse_col_arg(self, col, header_rev):
            """
            given a column index as string or a column name, returns the column index as integer;   
            in the latter case it is required a dictionary mapping each column name to its index
            """
            assert isinstance(col, int) or isinstance(col, str)
            assert isinstance(header_rev, dict)

            col_pos = None

            if col.isdigit():
                col_pos = int(col)
            else:
                if col not in header_rev:
                    raise CommandAbort("Cannot find column '%s'." % col)
                col_pos = header_rev[col]

            assert col_pos != None

            return col_pos

        
            

    def load_data(self, in_table_path, label_pos):
        """
        Input: a string representing a path of an existing csv file and an integer
                specifying the position of the label that splits data rows into
                different series
        Output: a pair (list,dictionary) where list contains column names, and
                dictionary contains pairs: label -> tuple; lable is a string
                while tuple is a tuple of data values
        """
        assert os.path.exists(in_table_path)
        assert isinstance(label_pos, int)
        assert label_pos >= 0

        data = {}   
        headers = []
 

        with open(in_table_path,"r") as in_table:
            for line in in_table:
    
                line = line.strip("\n")

                if len(line) == 0:
                    continue
                elif line.startswith("#"):

                    # take the first commented line as column headers
                    if len(headers) == 0:
                        headers = line.split(",")

                    continue

                row = line.split(",")

                if label_pos > len(row):
                    raise CommandAbort("The position of label (%s) is too big: the row has only %s elements" % (label_pos, len(row)))

                label = row[label_pos]

                if label not in data:
                    data[label] = []

                data[label].append(tuple(row))

        return (headers, data)

    @staticmethod
    def get_graph_spec(i):
 
        n_spec_color = len(Graph.SPEC_COLOR)
        n_spec_marker = len(Graph.SPEC_MARKER)
        n_spec_line = len(Graph.SPEC_LINE)

        id_spec_color = i / (n_spec_marker * n_spec_line)
        id_spec_marker = (i - (id_spec_color * n_spec_color * n_spec_line)) / n_spec_line
        id_spec_line = i - (id_spec_color * n_spec_color * n_spec_line) - (id_spec_marker * n_spec_line)

        spec_color = Graph.SPEC_COLOR[id_spec_color]
        spec_line = Graph.SPEC_LINE[id_spec_line]
        spec_marker = Graph.SPEC_MARKER[id_spec_marker]

        return "%s%s%s" % (spec_color, spec_line, spec_marker)


    def get_data_column(self, rows, col_pos):

        assert isinstance(col_pos, int)

        if col_pos < 0:
            raise CommandAbort("The column position must be a non-negative integer. Passed '%s'" % col_pos)

        col_values = map(lambda d: d[col_pos], rows)

        return col_values


    def run(self):

        in_table_path = self.get_arg("in_table")       

        (file_name, ext) = os.path.splitext(in_table_path)
        out_octave_path = self.get_arg("out_model") or file_name + ".m"

        if os.path.exists(out_octave_path) and not self.get_arg("force"):
            raise CommandAbort("The output file '%s' already exists. Either move it or use the -f/--force flag" % out_octave_path)

        if not os.path.exists(in_table_path):
            raise CommandAbort("The input file '%s' does not exist." % in_table_path)

        label_pos = self.get_arg("label")

        (headers,data) = self.load_data(in_table_path, label_pos)
 

        (x_pos, y_pos_list) = self.parse_cols_args(headers)

        figures = []


        for y_pos in y_pos_list:

            data_x = []
            data_y = []
            plot_spec = []

            for (i, (label,rows)) in enumerate(sorted(iter(data.items()))):

#                self.logger.debug("Label: %s" % label)

                y_series = self.get_data_column(rows, y_pos)
                data_y.append("y_%s = [ %s ];" % (label, ", ".join(y_series)))

                x_series = self.get_data_column(rows, x_pos)
                data_x.append("x_%s = [ %s ];" % (label, ", ".join(x_series)))


            
                graph_spec = self.get_graph_spec(i)

                plot_spec.append("x_%s,y_%s,'%s'" % (label, label, graph_spec))

            data_y = "\n".join(data_y)
            data_x = "\n".join(data_x)
            label_x = headers[x_pos].replace("_"," ")
            label_y = headers[y_pos].replace("_"," ")
            plot_spec = ",".join(plot_spec)

            legend = "'%s'" % "','".join(sorted(data.keys())).replace("_"," ")

            curr_figure = {
                "data_y": data_y,
                "label_y": label_y,
                "plot_spec": plot_spec,
            }

            figures.append(curr_figure)

        figure_filename = os.path.splitext(out_octave_path)[0]

        ctx = {
            "figures": figures,
            "data_x": data_x,
            "label_x": label_x,
            "legend": legend,
            "fig_cols": self.FIG_COLS,
            "fig_rows": int(math.ceil(float(len(figures)) / self.FIG_COLS)),
            "filename": figure_filename,
        }

        self.render_template(ctx, out_octave_path)

        if not self.get_arg("silent"):
            self.run_octave(out_octave_path)

    def run_octave(self, out_file_path):
        
        cmd = self.registry.get("render")

        cmd.execute(args=[out_file_path])


    def render_template(self, ctx, out_file_path):

        if not os.path.exists(self.OCTAVE_TEMPLATE_PATH):
            raise CommandAbort("The octave template path '%s' does not exist" % self.OCTAVE_TEMPLATE_PATH)

        template_dir = os.path.dirname(self.OCTAVE_TEMPLATE_PATH)
        template_name = os.path.basename(self.OCTAVE_TEMPLATE_PATH)

        t_loader = jinja2.FileSystemLoader(searchpath=template_dir)
        t_env = jinja2.Environment(loader=t_loader)
        t = t_env.get_template(template_name)

        model = t.render(ctx)
    
        with open(out_file_path, "w+") as out_file:
            out_file.write(model)
    
