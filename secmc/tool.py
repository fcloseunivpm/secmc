#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import argparse
import logging
import subprocess
import re
import time
import glob
import asteval
import math

from secmc.plugins.run_batch import RunBatch
from secmc.plugins.compute_metrics import ComputeMetrics
from secmc.plugins.compute_probability import ComputeProbability
from secmc.plugins.graph import Graph
from secmc.plugins.render import Render
from secmc.plugins.prompt import Prompt
from secmc.plugins.create_model import CreateModel
from secmc.plugins.split import SplitAndJoin
from secmc.plugins.run_batch_dist import RunBatchDist
from secmc.plugins.start_scheduler import StartScheduler
from secmc.plugins.stop_scheduler import StopScheduler
from secmc.plugins.hello_world import HelloWorld

def execute():
    from secmc import plugins

    reg = plugins.CommandRegistry()
    
    reg.add("compute_metrics", ComputeMetrics)
    reg.add("compute_probability", ComputeProbability)
    reg.add("graph", Graph)
    reg.add("run_batch", RunBatch)
    reg.add("render", Render)
    reg.add("prompt", Prompt, is_default=True)
    reg.add("create_model", CreateModel)
    reg.add("split", SplitAndJoin)
    reg.add("run_batch_dist", RunBatchDist)
    reg.add("start_scheduler", StartScheduler)
    reg.add("stop_scheduler", StopScheduler)
    reg.add("hello_world", HelloWorld)
    
    reg.execute()
