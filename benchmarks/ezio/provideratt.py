from itertools import chain, combinations
from decimal import Decimal
import os
import subprocess
import jinja2
import math
import sys
import dispy 
import shutil
import time

### TODO ###
#
# Add a custom Decimal class, that is either a wrapper around float numbers (IEEE double precision
# floating point numbers), or around the decimal.Decimal class (arbitrary precision), depending 
# on a configuration parameter
#
############

### BEGIN SCRIPT PARAMETERS

# "first-level" parameters, defined as constants in the program

PARAMS = {
    # the path of the parametric Markov Chain (in Prism format)
    "TEMPLATE_PATH" : "templates/aont-provideratt-conditional.prism.template",

    # the path of the cherry abstraction for the parametric Markov Chain (in Prism format)
    "TEMPLATE_PATH_CHERRIES" : "templates/aont-provideratt-cherries.prism.template",

    # the path of the specification (in Prism PCTL* format)
    "SPEC" : "spec/confidentiality.props",

    # incremental values of slices
    "N_SLICES" : [ 10,50,100,500,1000,1500 ],

    # the ratio of k/n; it determines the actual value of parameter k for the template Markov Chain
    # i.e. k = (k/n) * n, for every value of n \in N_SLICES
    "K_RATIO" : "0.70",

    # the i-th item is the probability of gaining access to the i-th provider
    "ATTACK_PROB" : [ 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50 ], # [ 0.10, 0.20, 0.30, 0.40, 0.50] #
    
    # the i-th item is the probability of dispatching a slice to the i-th provider;
    # the sum of all values should equal 1
    "DISPATCH_PROB" : [ 0.20, 0.20, 0.10, 0.10, 0.10, 0.10, 0.05, 0.05, 0.05, 0.05 ],  #  [ 0.2, 0.2, 0.2, 0.2, 0.2, ] 

    # results in interval [0,EPSILON] are approximated to zero; results in interval [1-EPSILON,1] are
    # approximated to 1; in both cases, once a result is approximated, it is not computed anymore for
    # bigger values of "n", saving the computation time of running a Prism instance
    "EPSILON" : Decimal("1E-07"), #Decimal("1E-05") 

}

# "second-level" parameters, defined as functions of "first-level" parameters
PARAMS.update({
    # the number of providers in the system
    "N_PROVIDERS" : len(PARAMS["ATTACK_PROB"]),
    
    # the unique names of the providers
    # the identifier of providers should be > 0
    "PROVIDER_IDS" : range(1, len(PARAMS["ATTACK_PROB"]) + 1),
    
    
    # a working directory where all working files (mainly actual Prism models) are saved 
    "WORKDIR" : "out-dispy-%s-%s" % (int(Decimal(PARAMS["K_RATIO"]) * 100), len(PARAMS["ATTACK_PROB"])),
    
})

### SANITY CHECKS ON SCRIPT PARAMETERS

assert len(PARAMS["DISPATCH_PROB"]) == len(PARAMS["ATTACK_PROB"])
assert len(PARAMS["DISPATCH_PROB"]) == PARAMS["N_PROVIDERS"]
assert len(PARAMS["PROVIDER_IDS"]) == PARAMS["N_PROVIDERS"]
assert sum(PARAMS["DISPATCH_PROB"]) == 1
assert not os.path.exists(PARAMS["WORKDIR"]), "Expect not existing workdir: %s" % PARAMS["WORKDIR"]
assert os.path.exists(PARAMS["TEMPLATE_PATH"]), "Expected existing template path: %s" % PARAMS["TEMPLATE_PATH"]

### AUXILIARY FUNCTIONS 

def parts(set):
    """
    Given a set A as input, returns the set of all subsets of A as output
    """
    res = chain.from_iterable(combinations(set,n) for n in range(len(set)+1))
    return res

#def prism_model_check(template_path, spec, env, workdir, id):
def prism_model_check(template_path, spec, env, id):
    """
    Taken as input a template file with the parametric Markov Chain (template), a file containing
    the PCTL* reachability specification to check (spec), a dictionary mapping the template variables
    to actual values (env), the working subdirectory where the concrete Prism model file and 
    output/error files should be stored (workdir), and a unique identifier of this invokation of
    prism (id), it returns the probability computed by Prism of the actual model satisfying the
    passed reachability specification.

    Being a probability, the output is comprised in the interval [0,1].

    NB at the moment this function requires a linux-like system, or at least an OS providing
    the command "grep", used to extract the probability computed by Prism from the output file.
    """

    import os
    import subprocess
    import jinja2
    from decimal import Decimal

    workdir = os.getcwd()
    assert os.path.exists(spec), "Expected existing spec file: %s" % spec
    assert os.path.exists(workdir), "Expected existing workdir: %s" % workdir

    # create the prism model
    jenv = jinja2.Environment(loader=jinja2.FileSystemLoader('.'))
    template = jenv.get_template(template_path)
    model_content = template.render(**env)
    model_path = os.path.join(workdir, "%s.prism" % id)
    model_out_path = "%s.out" % model_path
    model_err_path = "%s.err" % model_path

    with open(model_path, "wb") as the_model:
        the_model.write(model_content)

    with open(model_out_path, "w") as model_out:
        with open(model_err_path, "w") as model_err:    
            subprocess.call("prism %s %s" % (model_path, spec), shell=True, stdout=model_out, stderr=model_err)

    out = subprocess.check_output("grep Result %s | cut -d ':' -f 2 | cut -d ' ' -f 2" % model_out_path, shell=True)

    assert os.path.exists(model_path), "Expected model file to exist: %s" % model_path
    assert os.path.exists(model_out_path), "Expected model out file to exist: %s" % model_out_path
    assert os.path.exists(model_err_path), "Expected model err file to exist: %s" % model_err_path

#    print "model path: %s" % model_path

    dispy_send_file(model_path)
    dispy_send_file(model_out_path)
    dispy_send_file(model_err_path) # this should be done only if errors occur
    return (out.strip(), os.path.basename(model_path), os.path.basename(model_out_path), os.path.basename(model_err_path))

def get_attack_probability(strategy, providers):
    """
    Given a subset of providers (strategy) and a list of all the providers in the system (providers),
    it returns the probability associated to attacking exactly the providers contained in "strategy".

    Being a probability, the output is contained in the interval [0,1].
    """
    print "strategy: %s" % (strategy,)
    res = Decimal("1")
    for id in providers:

        if id in strategy:
            prob = Decimal(str(PARAMS["ATTACK_PROB"][id-1]))
        else:
            prob = Decimal("1") - Decimal(str(PARAMS["ATTACK_PROB"][id-1]))

        print "id: %s. prob: %s" % (id, prob)

        res = res * prob
       
    return res

def tuple_key(a):
    """
    Return a key for a generic tuple. This function can be passed to sorted(...) or similar 
    functions, in order to sort any iterable collection of tuples first
    by length, and next by its content, lexicographically.
    """
    return (len(a), a)

def log_status(id, num_attacks, attacked_providers, prob_conditioned, prob_attack, partial):
    print "%s/%s. attacked providers: %s. Conditioned probability: %s. Attack probability: %s. Partial: %s" % (id, num_attacks, attacked_providers, prob_conditioned, prob_attack, partial)
    sys.stdout.flush()

def enum_attack_strategies(attack_strategies):

    # zero_strategies contain all the strategies that, for some concrete value of "n", produced a
    # probability close to zero of attacking the confidentiality; the underlying assumption is that
    # it will produce a probability even closer to zero for bigger values of "n", thus we can skip
    # running Prism instances in the future, and immediately return the expected result (zero)
    zero_strategies = set([])
    
    # one_strategies collect all the strategies that, for some concrete value of "n", produced an
    # probability close to one (for the same reason above)
    one_strategies = set([])
    
    print PARAMS["ATTACK_PROB"]
    
    # enumerate attack strategies; for each of them compute the probability of the attacker realising 
    # that strategy, and store such probability for later use
    attack_probabilities = []
    prob_strategies = Decimal("0")
    for curr in sorted(attack_strategies, key=tuple_key):
        prob = get_attack_probability(curr, PARAMS["PROVIDER_IDS"])
    
        assert 0 <= prob and prob <= 1, "Wrong probability value %s for attack %s" % (prob, curr)
    
        attack_probabilities.append((curr, prob))
    
        prob_strategies = prob_strategies + prob    
    
        print "attack: %s. probability: %s" % (curr, prob)
        if prob < PARAMS["EPSILON"]:
            zero_strategies.add(curr)
        elif prob > Decimal("1") - PARAMS["EPSILON"]:
            one_strategies.add(curr)
 
    num_attacks = len(attack_probabilities)
    print "Cumulative probability of %s attack strategies: %s" % (num_attacks, prob_strategies)
    assert prob_strategies == Decimal("1"), "Expected 1, computed: %s" % prob_strategies


    return attack_probabilities, zero_strategies, one_strategies
    

def mc_concrete(attack_probabilities, zero_strategies, one_strategies):

    num_attacks = len(attack_probabilities)

    # repeat the model checking problem on concrete Markov Chains with bigger and bigger 
    # values of parameter "n" (number of slices to be stored)
    for curr_n in PARAMS["N_SLICES"]:
    
        print "epsilon: %s. zero strategies: %s. one strategies: %s." % (PARAMS["EPSILON"], zero_strategies, one_strategies)
    
        env = {
            # number of slices
            "n": curr_n,
            "k": int(math.floor(Decimal(PARAMS["K_RATIO"]) * curr_n)),
            # number of providers
            "m": PARAMS["N_PROVIDERS"],
            # attack probabilities
            "q": PARAMS["ATTACK_PROB"],
            # dispatch probabilities
            "d": PARAMS["DISPATCH_PROB"],
        }
    
        # create one sub-dir for each value of "curr_n"
        curr_workdir = os.path.join(PARAMS["WORKDIR"], str(curr_n))
        os.mkdir(curr_workdir)
    
    #    # prepare dictionary for collecting results of jobs
    #    prob_scenarios = {}
        # prepare the cluster with a bunch of jobs
        cluster = dispy.JobCluster(prism_model_check, depends=[PARAMS["TEMPLATE_PATH"],PARAMS["SPEC"]], cleanup=True)
        jobs = []
    
        tot = Decimal("0")
    
        for (id, (attacked_providers, p_conditional)) in enumerate(attack_probabilities):
    
            if attacked_providers in one_strategies:
                tot = tot + p_conditional
                log_status(id, num_attacks, attacked_providers, p_conditional, p_conditional, tot)
            elif attacked_providers in zero_strategies:
                log_status(id, num_attacks, attacked_providers, 0, p_conditional, tot)
            else:
                job_env = dict(env)
                job_env["attack"] = attacked_providers
    
                # add a new job to the cluster
                job = cluster.submit(PARAMS["TEMPLATE_PATH"], PARAMS["SPEC"], job_env, id)
                job.id = id
                jobs.append((job, p_conditional))
    
    
        # fetch the jobs and run them
        for (job, p_conditional) in jobs:
    
            job()
    
            prism_res, model_path, model_out_path, model_err_path = job.result
            prism_res = Decimal(prism_res)
    
            assert prism_res >= Decimal("0")
            assert prism_res <= Decimal("1")
    
            assert not os.path.isabs(model_path)
            assert not os.path.isabs(model_out_path)
    
            assert os.path.exists(model_path)
            assert os.path.exists(model_out_path)
    
            shutil.move(model_path, curr_workdir)
            shutil.move(model_out_path, curr_workdir)
    
            assert not os.path.exists(model_path)
            assert not os.path.exists(model_out_path)
    
            if os.path.exists(model_err_path):
                assert not os.path.isabs(model_err_path)
                shutil.move(model_err_path, curr_workdir)
                assert not os.path.exists(model_err_path)
        
            if job.stdout:
                print "=== JOB OUTPUT ==="
                print job.stdout
                print "=== END JOB OUTPUT ==="
    
            if job.exception:
                raise ValueError("Exception running the job: %s" % job.exception)
    
            if job.stderr:
                raise ValueError("Error running the job: %s" % job.stderr)
            
    
            id = job.id
    
            res = p_conditional * prism_res
    
            # sanity check on the probability read from Prism output file
            assert 0 <= res and res <= Decimal("1")
        
            curr = attack_strategies[id]
    
            if prism_res < PARAMS["EPSILON"]:
                # current strategy has a negligible probability, store as zero-strategy for saving time in the future
                zero_strategies.add(curr)
            elif prism_res >= Decimal("1") - PARAMS["EPSILON"]:
                # current strategy is very close to one, store as one-strategy for saving time in the future
                one_strategies.add(curr)
    
            tot = tot + res
    #        print "attack: %s. Prob.: %s (prob. of attack: %s)" % (curr, res, p_conditional)
            log_status(id, num_attacks, curr, prism_res, p_conditional, tot)
    
        
        # sanity check on the cumulative probability  
        assert Decimal("0") <= tot and tot <= Decimal("1"), "Expected 0 <= tot <= 1. Got: %s" % tot
        print "%s;%s;%s" % (PARAMS["N_PROVIDERS"], curr_n, tot)
        sys.stdout.flush()
        cluster.print_status()

def get_expected_attack_prob(attack_vector):
    """
    This computes the cherry-abstraction of the probability of having >= k slices with n 
    going towards infinite.

    Case 1: \sum_{i \in attack_vector} DISPATCH_PROB[i] > k/n => return 1
    Case 2: \sum_{i \in attack_vector} DISPATCH_PROB[i] < k/n => return 0
    Case 3: \sum_{i \in attack_vector} DISPATCH_PROB[i] = k/n => return 0.5

    """
    #print "attack_vector: %s; distribution function: %s" % (attack_vector, PARAMS["DISPATCH_PROB"])

    exp_dist = Decimal("0")
    for (pos, prob) in enumerate(PARAMS["DISPATCH_PROB"]):
        # the provider identifier is pos+1
        if (pos+1) in attack_vector:
            exp_dist = exp_dist + Decimal(str(prob))

    assert Decimal("0") <= exp_dist
    assert exp_dist <= Decimal("1"), "Expected 'exp_dist' <= 1. Got: %s" % (exp_dist,)

    k_ratio = Decimal(PARAMS["K_RATIO"])
    #print "exp dist: %s (%s). K_RATIO: %s (%s)" % (exp_dist, type(exp_dist), k_ratio, type(k_ratio))
    if exp_dist > k_ratio:
        exp_prob = Decimal("1")
    elif exp_dist == k_ratio:
        exp_prob = Decimal("0.5")
    else:
        exp_prob = Decimal("0")

    return exp_prob

def mc_cherries(attack_probabilities):
   
    num_attacks = len(attack_probabilities)

    env = {
        # number of providers
        "m": PARAMS["N_PROVIDERS"],
        # attack probabilities
        "q": PARAMS["ATTACK_PROB"],
        # dispatch probabilities
        "d": PARAMS["DISPATCH_PROB"],
    }

    tot = Decimal("0")

    # below there is no need to call prism, we already decomposed the problem and know the
    # probability of every attack vector, that is thus multiplied to the expected ratio of slices that
    # will be distributed
    for (id, (attacked_providers, p_conditional)) in enumerate(attack_probabilities):

        exp_succ_prob = get_expected_attack_prob(attacked_providers)

        res = p_conditional * exp_succ_prob

        # sanity check on the probability read from Prism output file
        assert 0 <= res and res <= Decimal("1")

        tot = tot + res
        log_status(id, num_attacks, attacked_providers, exp_succ_prob, p_conditional, tot)

    
    # sanity check on the cumulative probability  
    assert Decimal("0") <= tot and tot <= Decimal("1"), "Expected 0 <= tot <= 1. Got: %s" % tot
    print "%s;cherries;%s" % (PARAMS["N_PROVIDERS"], tot)
    sys.stdout.flush()

    
    

### START ACTUAL WORKFLOW

print "parameters: %s" % (PARAMS,)

os.mkdir(PARAMS["WORKDIR"])

attack_strategies = list(parts(PARAMS["PROVIDER_IDS"]))

attack_probabilities, zero_strategies, one_strategies = enum_attack_strategies(attack_strategies)  
 
mc_cherries(attack_probabilities)
#sys.exit(0) 

mc_concrete(attack_probabilities, zero_strategies, one_strategies)

