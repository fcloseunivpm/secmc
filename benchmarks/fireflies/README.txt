Instructions
============

In order to run the batches serializing their execution, launch the following command:

$ cd benchmarks/fireflies
$ secmc run_batch fireflies.batch

In order to run the batches distributing the workload over the available CPUs/nodes, launch the following commands:

On Machine A:
$ secmc start_scheduler --nodes <IP_A>

On Machine B
$ cd benchmarks/fireflies
$ secmc run_batch_dist anonimity.batch
$ secmc run_batch_dist --nodes <IP_A> correctness.batch

When Machine B returns (after receiving the results from Machine A), execute the following on Machine A:
$ secmc stop_scheduler

Note that in case Machine A and B are the same machine, <IP_A> can be the loopback address 127.0.0.1

Credits:

https://www.prismmodelchecker.org/casestudies/dining_crypt.php
