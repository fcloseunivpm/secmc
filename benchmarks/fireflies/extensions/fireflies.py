import numpy
import math
import itertools
import operator
import math
import decimal

import logging

logger = logging.getLogger()

#===============================================================================
def calculate_binomials(n, t):
    factorials = [1]
    for i in range(1, n + t + 1):
        factorials.append(i * factorials[i - 1])
    binomials = numpy.empty([n + t + 1, n + t + 1], dtype = int)
    binomials[0][0] = 1
    for i in range(1, n + t + 1):
        for j in range(0, i + 1):
            binomials[i][j] = factorials[i] / (factorials[j] * factorials[i - j])
    return binomials

#===============================================================================

#===============================================================================    

def combinations_with_replacement_counts(n, r):
   size = n + r - 1
   for indices in itertools.combinations(range(size), n-1):
       starts = [0] + [index+1 for index in indices]
       stops = indices + (size,)
       yield tuple(map(operator.sub, stops, starts))

#===============================================================================

#===============================================================================

def build_alphas(n, t, epsilon):

#    print("build_alphas(%s,%s,%s)" % (n,t,epsilon))
    global min_alphas, max_alphas
#    print("dopo global...")
    min_alphas = numpy.empty([t + 1, t + 1], dtype = int)
    max_alphas = numpy.empty([t + 1, t + 1], dtype = int)
#    print("dopo numpy ...")
    l = []
    s_prime = -1
    for i in range(1, t):
#        print("dentro for: %s/%s ..." % (i,t))
        alpha = 0
        minimum = alpha
#        print("prima di evolution ...")
        s = evolution(i) + int(round_half_up(perturbation(t, i, epsilon, alpha)))
        s = evolution(i)
#        print("prima di while...")
        while(s <= t and alpha < n):
            alpha = alpha + 1
            s_prime = evolution(i) + int(round_half_up(perturbation(t, i, epsilon, alpha)))
            maximum = alpha
            if s != s_prime:
                min_alphas[i][s] = minimum
                max_alphas[i][s] = maximum - 1                                
                l = l + [(i, s)]
                minimum = maximum
                s = s_prime
        if s > t:
            min_alphas[i][1] = minimum;
            max_alphas[i][1] = n;
            l = l + [(i, 1)]
        else:
            min_alphas[i][s] = minimum;
            max_alphas[i][s] = maximum;        
            l = l+ [(i, s)]
#    print("dopo primo for ...")
    for i in range (1, t + 1):
        for j in range(1, t + 1):
            if not ((i, j) in l) and (j > i or j == 1):
                if i == t:
                    min_alphas[i][j] = 0;
                else:
                    min_alphas[i][j] = n + 1;    
                max_alphas[i][j] = n + 1;

#===============================================================================

#==============================================================================
def evolution(x):
    return x + 1
#===============================================================================

#==============================================================================
def perturbation(t, x, epsilon, alpha):
    return x * epsilon * alpha
#==============================================================================

#==============================================================================
def round_half_up(x):
#    print("round_half_up(%s:%s)" % (x,type(x)))
#    return math.ceil(x) if x - math.floor(x) >= 0.5 else math.floor(x)
    return x.quantize(1, rounding=decimal.ROUND_HALF_UP)

#    raise Exception("TODO ...")
#==============================================================================



def build_f_vector(N, T, EPSILON, RP):
    f_vector = []
    for i in range(pow(2,N)): 
        v = '_'.join(tuple('{:0{}b}'.format(i, N)))
        n_ones = v.count('1')
        row = { "VECTOR":v, "N_ONES":n_ones, }
        f_vector.append(row)

    return f_vector


def build_population(N, T, EPSILON, RP):
    population = []
    binomials_res=calculate_binomials(N, T)
    num_tuples = 0;
    config = 0;
    for mytuple in combinations_with_replacement_counts(T, N):
        num_tuples = num_tuples + 1;
    it_count = 0;
    for mytuple in combinations_with_replacement_counts(T, N):
        it_count = it_count +1;
        config_1 = 0;
        sigma_n_i = 0;        
        binomial_product = 1;
        for i in range(T):
            binomial_product *= binomials_res[N - sigma_n_i, mytuple[i]];
            sigma_n_i = sigma_n_i + mytuple[i];
        for i in range(T - 1):
            if it_count == num_tuples:
                config_1 = mytuple[T-1];
        row = {
            "N_EVENTS": binomial_product,
            "CONFIG": [mytuple[i] for i in range(T - 1)],
            "CONFIG_1": mytuple[T-1],
        }
        population.append(row)
    return population   


def build_rows(N, T, EPSILON, RP):
#    print("Enter build rows(%s,%s,%s,%s) ..." % (N,T,EPSILON,RP))
    rows = []
    
#    print("Before build alphas(%s,%s,%s)" % (N,T,EPSILON))
    build_alphas(N, T, EPSILON)
#    print("After build alphas ...")
    masks = numpy.empty((pow(2,N,)), dtype = list)
    pr_strings = numpy.empty((pow(2,N,)), dtype = object)
    pow_2_n = pow(2, N)
    for r in range(pow_2_n):
        bin_r = '{:0{}b}'.format(r, N)
        pr_strings[r] = 'pr_%s' % ('_'.join(bin_r))
        masks[r] = list(map(int, '{:0{}b}'.format(r, N)))
    
    for sigma in combinations_with_replacement_counts(T, N):
        if (sigma[T -1] != 0):
            next_sigmas = []
            pr_string_list = []
            for r in range(pow_2_n):
                mask = masks[r]
                next_sigma = numpy.zeros((T,), dtype = int)
                flashed = numpy.zeros((T,), dtype = int)    
                for i in range(T, 0, -1):
                    flashed[i - 1] = sigma[i - 1] - sum(mask[:sigma[i - 1]])
                    mask = mask[sigma[i - 1]:]
                alpha = 0
                for i in range(T -1, -1, -1):
                    s = i + 1
                    if s > RP:
                        for k in range(T, i, -1):
                            s_primed = (k % T) + 1
                            if min_alphas[s][s_primed] <= alpha <= max_alphas[s][s_primed]:
                                next_sigma[s_primed - 1] += sigma[s - 1]
                                if s_primed == 1:
                                    alpha += flashed[s - 1]
                                    break
                    else:
                        next = (i + 1) % T
                        next_sigma[next] += sigma[s - 1]
                index = [i for i, j in enumerate(next_sigmas) if numpy.array_equal(next_sigma, j)]
                if not index:
                    next_sigmas.append(next_sigma)
                    pr_string_list.append(pr_strings[r])
                else:
                    pr_string_list[index[0]] += (' + %s' % pr_strings[r])
            l = len(next_sigmas)
            row = {
                "SIGMA": sigma,
                "LEN_NEXT_SIGMAS" : len(next_sigmas),
                "NEXT_SIGMAS": next_sigmas,
                "PR_STRING_LIST": pr_string_list,
            }
            
            rows.append(row)

     
#    print("Num rows: %s" % len(rows))   
    assert len(rows) > 0, "The given parameters don't generate any row"
    return rows
