---
common:
    imports:
        - name: fireflies
          alias: fireflies
          path: ./extensions/fireflies.py
        - name: decimal.Decimal
          alias: Decimal 

    template:
        value: "templates/fireflies.prism.template"
        check:
            - path_exists

    specs:
        value: "specs/sync.pctl"
        check:
            - path_exists

    timeout: 240m
    N:
        value: 6
        description: number of fireflies in the system
    T: 
        value: 11
        description: internal phases of each firefly

    EPSILON:
        value: Decimal("0.1")
        description: coupling strength between fireflies

    ML:
        value: Decimal("0.1")
        description: message loss probability

    RP:
        value: 10
        description: refractory period

    F_VECTOR:
        exp: fireflies.build_f_vector(N, T, EPSILON, RP)
        description: builds all possible failure vector given the specified parameters 
        depends: [ N, T, EPSILON, RP ]
        

    POPULATION:
        exp: fireflies.build_population(N, T, EPSILON, RP)
        description: builds the initial transition given all specified parameters
        depends: [ N, T, EPSILON, RP ]

    ROWS:
        exp: fireflies.build_rows(N, T, EPSILON, RP)
        description: builds all the update transitions when an oscillator flashes
        depends: [ N, T, EPSILON, RP ]
batches: 
    test:
        N:
            range:
                from: 6
                to: 10
                step: 1
            description: number of fireflies in the system
        T:
            range:
                from: 11
                to: 11
                step: 1
            description: internal phases of each firefly
        EPSILON:
            range:
                from: Decimal("0.1")
                to: Decimal("0.1")
                step: Decimal("0.1")
            description: coupling strength between fireflies
        ML:
            range:
                from: Decimal("0.1")
                to: Decimal("0.1")
                step: Decimal("0.1")
            description: message loss probability
        RP:
            range:
                from: 1
                to: 10
                step: 1
            description: refractory period
