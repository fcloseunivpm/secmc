#!/usr/bin/env python

from __future__ import division # this avoids using integer division
import scipy.special
import math

def summation(n, i, p):
    bin1 = scipy.special.binom(n, i)
    bin2 = scipy.special.binom(n-i,s-i)
    return bin1 * p**i * (1-p)**(n-i) * bin2 * p**(s-i) * (1-p)**(n-s)

#n = 5
n_values = [5, 10, 15, 20]
alpha = 2/3
p = 0.148


### itero n
for n in n_values:
    k = int(n*alpha)

    s_values = range(0, k) # 0, 1, 2, ..., k-1

    print "### n = %s, k = %s ###" % (n, k)
    ### itero s
    for s in s_values:
        ### sommatoria
        tot = 0
        partials = []
        for i in range(0,s+1):
            curr = summation(n, i, p)
            partials.append(curr)
            tot = tot + curr
    
        
        #print "partials (s=%s): %s" % (s, partials)
        print "tot (s=%s) = %s" % (s, tot)
    
    
