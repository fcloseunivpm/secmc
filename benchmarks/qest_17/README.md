# Reproduce experiments for paper submitted to QEST 2017

Here are the instructions to reproduce the data produced by experiments in a paper sent to the conference QEST 2017.


1. clone the repository: `$ git clone <REPO_URL>`
2. change directory to your local repository folder: `$ cd secmc`
3. install the tool: `$ pip install .`
4. check the tool is correctly installed: `$ secmc --help`
5. change directory to the QEST 17 benchmarks: `$ cd benchmakrs/qest_17`
6. check it does not exist a `out` sub-directory (otherwise the script will stop)
7. lunch the script with the assessment workflow: `$ secmc prompt -s main.script`
8. when the execution ends, there will be two files (`exp_wireless.csv` and `exp_combined.csv` containing the data in the paper)
9. compare the csv files you get with the ones contained in the `results` sub-directory (or the `results.ods` spreadsheet)
