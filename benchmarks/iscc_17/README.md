# Reproduce experiments for paper submitted to ISCC 2017

Here are the instructions to reproduce the data produced by experiments in the following paper:

* "Security in heterogeneous distributed storage systems: a practically achievable information-theoretic approach" 

appearing in the proceedings of the conference ISCC 2017.


1. clone the repository: `$ git clone <REPO_URL>`
2. change directory to your local repository folder: `$ cd secmc`
3. checkout the conference branch: `$ git checkout iscc_2017`
4. install the tool: `$ pip install .`
5. check the tool is correctly installed: `$ secmc --help`
6. change directory to the ISCC 17 benchmarks: `$ cd benchmakrs/iscc_17`
7. lunch the script with the experiments workflow: `$ secmc prompt -s main.script`
8. when the execution ends, there will be two files (`exp_k_n_ratio.csv` and `exp_non_uniform_allocation.csv` containing the probability data from which the paper graphs are obtained)
9. compare the csv files you get with the ones contained in the `results` sub-directory 
