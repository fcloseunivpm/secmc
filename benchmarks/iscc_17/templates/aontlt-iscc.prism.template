mdp

const int n = {{ n }};  // numero di slice da trasmettere
const int m = {{ m }}; // numero di server, e quindi di canali
const int k_1 = {{ k_1 }}; // num max di slice che non mi permettono di ricostruire il messaggio
const int k_2 = {{ k_2 }}; // num min di slice per ricostruire completamente il messaggio

// q_i : probability that attacker can intercept the slice while the client is transmitting it on channel i
{% for prob in q_i %}
const double q_{{ loop.index }} = {{ prob }};
{% endfor %}

// d_i : probability of distributing a slice on channel i
{% for prob in d_i %}
const double d_{{ loop.index }} = {{ prob }};
{% endfor %}

// x_i : probability that attacker can guess the message given it has catched i slices
{% for prob in x_i %}
const double x_{{ loop.index-1 }} = {{ prob }};
{% endfor %}

// g : probability that attacker can guess one slice
const double g = {{ p_guess }};


module client

// LOCAL STATE
    pc_c : [0..3] init 0;
    // pc_c=0 - inattivo
    // pc_c=1 - trasmissione
    // pc_c=2 - corrisponde allo stato pc_cb del pdf
    // s2=3 - ha inviato tutte e n le slice

// LOCAL COUNTER
    ctr : [0..n] init 0;

    c : [0..m] init 0; // numero di canali su cui trasmette il client (che corrispondono al numero di server)

    // pick a channel to send the slice to
    [] (pc_c=0) & (ctr<n) ->
{% for I in range(1,m+1) %}
        d_{{ I }} : (pc_c'=1) & (c'={{ I }}){% if not loop.last %} + {% else %};{% endif %}
{% endfor %}

    // check whether the chosen channel is faulty
{% for I in range(1,m+1) %}
    [] (pc_c=1) & (c={{ I }}) -> (pc_c'=2);
{% endfor %}

    // sends on channel
{% for I in range(1,m+1) %}
    [slice] (pc_c=2) & (c = {{ I }}) & (ctr<n) -> (pc_c'=0) & (ctr'=ctr+1) & (c'=0);
{% endfor %}

    // final step
    [] (pc_c=0) & (ctr=n) -> (pc_c'=3) & (c'=0);

endmodule


module attacker

    pc_a : [0..3] init 0;
    // pc_a = 0 : less than k_1 slices catched
    // pc_a = 1 : between k_1 and k_2 slices catched, but didn't try to recover the message
    // pc_a = 2 : between k_1 and k_2 slices catched, but didn't succeed to recover the message
    // pc_a = 3 : between k_1 and k_2 slices catched, and  recovered the message

    ctr_a : [0..k_2] init 0;

    // begin intercepting or guessing transitions
    // try to intercept; if not intercept, try to guess; if not guess, nothing changes

    // CASE 1: ctr_a < k_1 - 1
{% for I in range(1,m+1) %}
    [slice] (pc_a=0) & (ctr_a<k_1-1) & (c={{ I }}) -> q_{{ I }}:(ctr_a'=ctr_a+1) + ((1-q_{{ I }})*g):(ctr_a'=ctr_a+1) + ((1-q_{{ I }})*(1-g)):(pc_a'=pc_a) & (ctr_a'=ctr_a);
{% endfor %}

    // CASE 2: ctr_a = k_1 - 1 
{% for I in range(1,m+1) %}
    [slice] (pc_a=0) & (ctr_a=k_1-1) & (c={{ I }}) -> q_{{ I }}:(ctr_a'=ctr_a+1) & (pc_a'=1) + ((1-q_{{ I }})*g):(ctr_a'=ctr_a+1) & (pc_a'=1) + ((1-q_{{ I }})*(1-g)):(pc_a'=pc_a) & (ctr_a'=ctr_a);
{% endfor %}

    // CASE 3: k_1 <= ctr_a < k_2
{% for I in range(1,m+1) %}
    [slice] (pc_a=2) & (ctr_a<k_2) & (c={{ I }}) -> q_{{ I }}:(ctr_a'=ctr_a+1) & (pc_a'=1) + ((1-q_{{ I }})*g):(ctr_a'=ctr_a+1) & (pc_a'=1) + ((1-q_{{ I }})*(1-g)):(pc_a'=pc_a) & (ctr_a'=ctr_a);
{% endfor %}

    // end intercepting or guessing transitions

    // try to guess the message given ctr_a slices intercepted
{% for I in range(0,k_2-k_1+1) %}
    [] (pc_a=1) & (ctr_a=k_1 + {{ I }}) -> x_{{ I }}:(pc_a'=3) + (1-x_{{ I }}):(pc_a'=2);
{% endfor %}

    // final loop (avoids the client deadlocks)
    [slice] (pc_a=3) -> (pc_a'=3);


endmodule

label "hacked" = (pc_a=3);
