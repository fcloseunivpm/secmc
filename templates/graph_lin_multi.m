{{ data_x }}

figure;

{% for curr_figure in figures %}
    %% {{ curr_figure.label_y|upper }}
    subplot({{ fig_rows }}, {{ fig_cols }}, {{ loop.index }});

    {{ curr_figure.data_y }}


    plot({{ curr_figure.plot_spec }});
    xlabel('{{ label_x }}');
    ylabel('{{ curr_figure.label_y }}');
    legend({{ legend }});
    grid on;
{% endfor %}

print("{{ filename }}", "-dpng");
