{{ data_y }}

{{ data_x }}

figure;
%%plot(nvect, psim, "r*");
%%semilogy(nvect, psim, "r*");
semilogy({{ plot_spec }});
xlabel('{{ label_x }}');
ylabel('{{ label_y }}');
legend({{ legend }});
grid on;
