psim = [ <VALUES> ];

nvect = [1:length(psim)];

figure;
plot(nvect, psim, "r*");
xlabel('n');
ylabel('p');
legend('simulated');
grid on;
