try:
    from setuptools import setup, find_packages
except ImportError:
    raise ImportError("Install setup tools")

config = {
    'description': 'SecMC',
    'author': 'Francesco Spegni',
    'url': 'https://bitbucket.org/fcloseunivpm/secmc.git',
    'download_url': 'https://bitbucket.org/fcloseunivpm/secmc.git',
    'author_email': 'f.spegni@univpm.it',
    'version': '0.1',
    'install_requires': [
        'Jinja2>=2.8',  
        'MarkupSafe>=0.23',
        'argparse>=1.2.1',
        'subprocess32>=3.5.3',
        'asteval>=0.9.5',
        'argcomplete>=1.6.0',
        'python-daemon>=2.1.1',
        'prompt-toolkit>=1.0.8,<=1.0.15',
        'Pygments>=2.1.3',
        'daemon>=1.1',
        'dispy>=4.7.6',
        'psutil>=5.2.1',
        'lockfile>=0.12.2',
        'netifaces>=0.10.7',
        'future>=0.17.1',
        'PyYAML>=3.12'],
    'packages': find_packages(),
    'entry_points': {
        # the format of each console_script item is: name=package.module:function_name
        'console_scripts': [ 'secmc=secmc.tool:execute' ],
    },
    'name': 'secmc',
    'license': 'MIT',
    'classifiers': [
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',
    
        # Indicate who your project is intended for
        'Intended Audience :: Science/Research',
        'Topic :: Security',
        'Topic :: Scientific/Engineering',

        # Pick your license as you wish (should match "license" above)
         'License :: OSI Approved :: MIT License',
    
        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 2.7',
    ],
    'keywords': 'security model checking formal methods prism'    
}


setup(**config)

