# Installation

> These instructions have been tested on: Ubuntu, Debian, Mac OS X, with Python 2.7. We are aware of incompatibilities with Python 3 and Windows platforms. Eventually, we are going to fix them ...

* install python >= 2.7

* install prism >= 4.3 and make the commands `prism` and `xprism` available in your paths (you can test the correct configuration invoking `which prism`; it should return the path where the command `prism` has been installed)

* clone the repository: 
```
$ git clone <REPO_URL>
```

* install the tool: 
```
$ cd secmc && pip install .
```

* test installation was correct by printing the command help

```
$ secmc --help
```

# Usage
* generate the model for the Provider Attacker

```
$ secmc create_model templates/aontlt-provideratt.prism.template mymodel.prism '{ "n":<NUM>, "m":<NUM>, "sps":<NUM>, "k_1":<NUM>, "k_2":<NUM>, "p": [ <list of probabilities> ], "q": [ <list of probabilities> ], "x": [ <list of probabilities> ] }' 
```
* example of model generated for the Provider Attacker with concrete parameters
```
$ secmc create_model -f templates/aontlt-provideratt.prism.template mymodel.prism '{ "n":5, "m":5, "sps":10, "k_1":3, "k_2":4, "p": [ 1.0,1.0,1.0,1.0,1.0 ], "q": [0.1,0.2,0.3,0.4,0.5], "x": [ 0.5, 1.0 ] }'
```


* generate the model for the Slice Attacker
```
$ secmc create_model templates/aontlt-sliceatt-linear.prism.template mymodel.prism '{ "n":<NUM>, "m":<NUM>, "sps":<NUM>, "k_1":<NUM>, "k_2":<NUM>, "p": [ <list of probabilities> ], "q": [ <list of probabilities> ], "x": [ <list of probabilities> ], "d": [ <list of probabilities> ], "p_guess": <NUM> }'
```
* example of model generated for the Slice Attacker with concrete parameters

```
$ secmc create_model -f templates/aontlt-sliceatt-linear.prism.template mymodel.prism '{ "n":5, "m":5, "sps":10, "k_1":3, "k_2":4, "p": [ 1.0,1.0,1.0,1.0,1.0 ], "q": [0.1,0.2,0.3,0.4,0.5], "x": [ 0.5, 1.0 ], "d": [0.1,0.1,0.2,0.3,0.3], "p_guess": 0.00002 }'
```

* meaning of parameters:

    * _n_ : number of slices
    * _m_ : number of providers
    * _sps_ : capacity of providers (formerly: slice-per-server)
    * _k\_1_ : minimum number of slices for rebuilding the original message with positive probability
    * _k\_2_ : maximum number of slices for rebuilding the original message with probability 1
    * _p_ : list of probabilities that the i-th provider can receive a slice
    * _q_ : list of probabilities that the i-th provider can be attacked
    * _d_ : list of probabilities that the i-th provider is selected to receive a slice
    * _x_ : list of probabilities that the message can be reconstruct given (k_1 + i) slices have been collected
    * _p\_guess_ : probability of guessing a non-intercepted slice

* constraint between parameters:

    * _n_ <= _m_ * _sps_
    * 0 <= _k\_1_ <= _n_, 0 <= _k\_2_ <= _n_ (RS encoding requires _k\_1_ = _k\_2_, LT encoding allows _k\_1_ < _k\_2_)
    * 0 <= _p[i]_ <= 1
    * 0 <= _q[i]_ <= 1
    * len(_d_) = m, sum(_d_) = 1.0
    * len(_p_) = m
    * len(_q_) = m
    * len(_x_) = k\_2 - k\_1 + 1
    * 0 <= _p\_guess_ <= 1



* load the model on Prism:

```
$ xprism <FILE>
```

* example:

```
$ xprism mymodel.prism
```

# Develop

> This section explains how to write a custom command that can be invoked from the tool prompt. You can see the file `secmc/plugins/hello_world.py` for an example of how to create a custom command.

* create a new python module (e.g. `mycommand.py`) within `secmc.plugins`

* create a class (e.g. `MyCommand`) that extends `secmc.plugins.Command`, in the just created module

* the command should specify a `description` class attribute specifying some help text to be read by the user in case the help page is called, e.g.

````
    from secmc.plugins import BaseCommand

    class MyCommand(BaseCommand):
    
        description = "this is a hello-world command"

````

* the command should implement a `run` method taking no arguments, e.g.

````
    def run(self):
        print ("Hello world")
````

* the command may implement also a `get_args_parser` that takes no argument and returns a parser. Currently we use as parser an instance of `semc.plugins.CommandArgumentParser`, which in turn extends `argparse.ArgumentParser`, so you can refer to the documentation of the latter (e.g. [here](https://docs.python.org/2.7/library/argparse.html)). The method `secmc.plugins.Command.get_args_parser` of the base class for commands already return a parser taking a set of default arguments. Thus, the custom command can focus on specifying new arguments to accept. For instance:

````
        def get_args_parser(self):

            parser = super(MyCommand, self).get_args_parser()
            parser.add_argument("workdir", type=str, nargs="?", default="", help="the path where the results should be collected")
            
            parser.add_argument("--dry", action="store_true", help="check only that the batch file/s is/are correct (validation mode)")
            return parser
````
* now it's time to register your command in a registry containing all the commands that can be invoked by the prompt. The registry is managed by the `secmc.tools.execute` method. Just add a reference to your command class and specify the name with which your command can be invoked, e.g. in `secmc/tools.py`:

````
    def execute():
        from secmc import plugins

        reg = plugins.CommandRegistry()

        ...
    
        reg.add("my_command", plugins.MyCommand)

        ...
    
        reg.execute()
````
* It is possible to test the correct handling of the new command by invoking the tool `secmc`, which by default executes the prompt, and call the new command in the prompt:

````
    $ secmc 
    # this will open a prompt in the following

    secmc:> my_command --help

    This is a hello-world message
    ...
    
````

# Create a benchmark
We name _benchmark_ an application of the tool to model a real problem. It involves creating one _template_ (e.g. a template of a Prism model) of the probabilistic model to be explored under several assumptions on the parameter values, and one _specifications_ (e.g. a PCTL formula) to check on the models. It also requires to specify the intervals within which the parameters may vary.

The core of the benchmark is one or more `.batch` files that specify all such information. Each `.batch` file can specify the parameter values of one or more batches. We call _batch_ a set of executions where the parameters assume values in the specifed ranges and for each execution we compute the probability of the given specifiation. In this way it is possible to observe how the probability computed by the specification varies w.r.t. the batch parameters.

The syntax of the `.batch` file is YAML, so you may refer to the YAML documentation (e.g. [here](http://yaml.org/spec/1.2/spec.html)).

You may refer to the benchmark `benchmarks/dispy/exp_wireless.batch` to understand the structure of a `.batch` file. The batch file is composed of two main sections: `common` and `batches`. The former defines parameter that are common to all the batches, while the latter allows to specify a list of batch executions of the model checker.

Note that a `.batch` file shold specify, for every batch, a single model template path and a single model specification path. They can be specified once and for all in the `common` section, or once in every batch of the `batches` section.

Note also that the model template file and the specification template file should follow the syntax of Jinja2 template files (e.g. [here](http://jinja.pocoo.org/) for the documentation of Jinja2).

# TODO:

* test the code for compatibility with Python 3, and fix related bugs
* test the code for compatibility with Windows, and fix related bugs
